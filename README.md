# EPOS-IP GNSS EUROPE Database #

This document describes the database developed to store and disseminate raw data and metadata from GNSS stations operating in Europe. This project is part of [EPOS-IP WP10](https://www.epos-ip.org/tcs/gnss-data-and-products), a thematic core service of the European Plate Observing System that aims to promote integration of data, data products, and facilities from distributed research infrastructures for solid Earth science in Europe [EPOS-IP](https://www.epos-ip.org/). The document explains how to setup and deploy the latest database version.

**Authors:**
 - Rui Cardoso (rui.cardoso@segal.ubi.pt)
 - Tryggvi Hjörvar (tryggvi@vedur.is)
 - Quentin Baire (qbaire@oma.be)
 
 
**Date:** 16. January 2024

**Version:** V3-0-3

**Database Management System (DBMS):**
 - PostgresSQL version 15.5 (used in the development phase)
 - PostgreSQL version 12.0 (minimal requirements for production phase)

### Files in the project ###
Overview of the files maintained in the repository:
 
 - `README.MD` (this file).
 - `gnss-europe.dbs` (diagram-oriented file produced by DBSchema to manage, synchronize schemas and generate documentation).
 - `gnss-europe.png` (database schema in png format).
 - `gnss-europe.html` (database schema and documentation in html).
 - `gnss-europe.pdf` (database schema and documentation in pdf).
 - `gnss-europe.pgdump.sql` (database sql dump file with the latest release version).
 - `LICENCE` (project license type: Creative Commons Attribution-ShareAlike 4.0 International License).


### Overview and workflow ###
The workflow around the database development REQUIRES developers to regularly drop and recreate the database from the install script. There are no migration scripts maintained in this repository.

You need therefore to be able to rerun any local data imports or database initialization after a clean install.

NOTE that the database versioning is a part of the database name, and you must take care to connect to the new database after installation.
This means it is easy to compare your code running against different versions of the database schema, but it is recommended to DROP the old version as soon as development moves on to the next version.

### Versioning ###
The project uses [Semantic versioning 2.0.0](http://semver.org)

Given a version number MAJOR.MINOR.PATCH, increment the:

    MAJOR version when you make incompatible API changes,
    MINOR version when you add functionality in a backwards-compatible manner, and
    PATCH version when you make backwards-compatible bug fixes.

[Tags](https://gitlab.com/gpseurope/database/tags) should mostly cover any changes in versions, but some PATCH versions may be skipped.
Please reference the version TAG when opening an issue or in discussions.

The version number is included in the database name, e.g. "gnss-europe-vX-X-X", where the X's represent the version number.

### Installation ###
We assume you already have a PostgreSQL server set up with relevant user privileges.

1. CLONE the repository or download as a ZIP-file.
2. Run `psql < gnss-europe.pgdump.sql`

You should now have a clean install of the database, under the name "gnss-europe-vX-X-X", where the X's represent the version number.

* Update any scripts to point to the new database
* DROP the old database

### Documentation ###
The database documentation includes an overall database schema and database detail in different formats. The application employed is DBSchema, a diagram-oriented database tool used to design the database schema and produce reports (https://www.dbschema.com/). The schema includes two different sections of the database T1 and T4T5T6 in the file `gnss-europe.dbs`. 

### Database changes ###
Change requests MUST be submitted as [Issues](https://gitlab.com/gpseurope/database/issues). 
Decisions to adopt or reject changes and the arguments and reasons for and against should be documented with the relevant issue as much as makes sense.
Please reference the version TAG when opening an issue.


### Useful commands during development ###
* deploying a new database version from repository `psql < gnss-europe.pgdump.sql`
* dumping database with CREATE command `pg_dump --create -d gnss-europe-vX-X-X -O > gnss-europe.pgdump.sql`
* dumping data only from a named table `pg_dump -d gnss-europe-vx-X-X -a -t tablename > tablename.pgdump.sql`

### License ###

This project is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/). 

## Acknowledgments ###

* This project has received funding from the European Union's Horizon 2020 research and innovation program under grant agreement N° 676564