--
-- PostgreSQL database dump
--

-- Dumped from database version 15.5
-- Dumped by pg_dump version 15.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: pgstattuple; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgstattuple WITH SCHEMA public;


--
-- Name: EXTENSION pgstattuple; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION pgstattuple IS 'show tuple-level statistics';


--
-- Name: coloc_status; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.coloc_status AS ENUM (
    'PERMANENT',
    'MOBILE',
    'NONE'
);


--
-- Name: count_rows(text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.count_rows(schema text, tablename text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
    declare
      result integer;
      query varchar;
    begin
      query := 'SELECT count(1) FROM ' || schema || '.' || tablename;
      execute query into result;
      return result;
    end;
    $$;


--
-- Name: markerlongname(character varying, integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.markerlongname(marker character varying, mon integer, rec integer, cou character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
BEGIN
	IF cou IS NUll THEN
		cou = '000';
	END IF;
	IF rec IS NUll THEN
		rec = 0;
	END IF;
	IF mon IS NUll THEN
		mon = 0;
	END IF;
	return  marker  || to_char(mon,'FM9MI') ||  to_char(rec, 'FM9MI') || cou;
END;
$$;


--
-- Name: updateidsequences(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.updateidsequences() RETURNS void
    LANGUAGE plpgsql
    AS $$
	DECLARE
	    my_row    RECORD;
		aux 	text;
	    aux_tablename text;
	BEGIN       
	    FOR my_row IN 
	        SELECT table_name
	        FROM   information_schema.tables
	        WHERE  table_schema = 'public'
	        ORDER BY table_name ASC
	    LOOP
	        RAISE NOTICE 'Updating sequence ID on %', my_row.table_name;
			aux := my_row.table_name || '_id_seq';
			aux_tablename := my_row.table_name;
			if exists (select * from information_schema.sequences where information_schema.sequences.sequence_name = aux)
			then
				execute format('SELECT setval(%L, COALESCE((SELECT MAX(id)+1 from %I), 1), false)', aux, aux_tablename);
			else
				raise notice '  Could not run on %', aux;
			end if;
	    END LOOP;
	END;
$$;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: agency; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.agency (
    id integer NOT NULL,
    name text NOT NULL,
    abbreviation text DEFAULT NULL::character varying,
    address text DEFAULT NULL::character varying,
    www text DEFAULT NULL::character varying,
    infos text,
    pid text
);


--
-- Name: TABLE agency; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.agency IS 'Operational Center.';


--
-- Name: COLUMN agency.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.agency.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN agency.name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.agency.name IS 'Institutional name of the operational center.';


--
-- Name: COLUMN agency.abbreviation; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.agency.abbreviation IS 'Unique acronym associated to the operational center and used for its identification.';


--
-- Name: COLUMN agency.address; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.agency.address IS 'Institutional address.';


--
-- Name: COLUMN agency.www; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.agency.www IS 'Institutional website url.';


--
-- Name: COLUMN agency.infos; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.agency.infos IS 'Additional information to characterize the operational center.';


--
-- Name: COLUMN agency.pid; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.agency.pid IS 'persistent identifier';


--
-- Name: agency_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.agency_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: agency_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.agency_id_seq OWNED BY public.agency.id;


--
-- Name: SEQUENCE agency_id_seq; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON SEQUENCE public.agency_id_seq IS 'Auto increment sequence applied to agency primary key.';


--
-- Name: agency_network; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.agency_network (
    id integer NOT NULL,
    id_agency integer NOT NULL,
    id_network integer NOT NULL
);


--
-- Name: agency_network_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.agency_network_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: agency_network_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.agency_network_id_seq OWNED BY public.agency_network.id;


--
-- Name: attribute; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.attribute (
    id integer NOT NULL,
    name text NOT NULL
);


--
-- Name: TABLE attribute; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.attribute IS 'Specification of an item''s attributes (e.g., radome, antenna, filter).';


--
-- Name: COLUMN attribute.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.attribute.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN attribute.name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.attribute.name IS 'Attribute name.';


--
-- Name: attribute_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.attribute_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: attribute_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.attribute_id_seq OWNED BY public.attribute.id;


--
-- Name: authentication; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.authentication (
    id integer NOT NULL,
    user_id character varying(64),
    access_token character varying(64),
    expiration_date timestamp without time zone
);


--
-- Name: authentication_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.authentication_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bedrock; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bedrock (
    id integer NOT NULL,
    condition text NOT NULL,
    type text
);


--
-- Name: TABLE bedrock; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.bedrock IS 'Specification of type and condition of bedrock.';


--
-- Name: COLUMN bedrock.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bedrock.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN bedrock.condition; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bedrock.condition IS 'Characterization of bedrock condition.';


--
-- Name: COLUMN bedrock.type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.bedrock.type IS 'Specification of bedrock type.';


--
-- Name: bedrock_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bedrock_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bedrock_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bedrock_id_seq OWNED BY public.bedrock.id;


--
-- Name: condition; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.condition (
    id integer NOT NULL,
    id_station integer NOT NULL,
    id_effect integer NOT NULL,
    date_from timestamp without time zone DEFAULT ('now'::text)::timestamp without time zone,
    date_to timestamp without time zone,
    degradation text,
    comments text
);


--
-- Name: TABLE condition; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.condition IS 'Condition and effects applied to station.';


--
-- Name: COLUMN condition.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.condition.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN condition.id_station; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.condition.id_station IS 'Reference (foreign key) from table station.';


--
-- Name: COLUMN condition.id_effect; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.condition.id_effect IS 'Reference (foreign key) from table effects.';


--
-- Name: COLUMN condition.date_from; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.condition.date_from IS 'Start date of the specific condition.';


--
-- Name: COLUMN condition.date_to; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.condition.date_to IS 'End date of the specific condition.';


--
-- Name: COLUMN condition.degradation; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.condition.degradation IS 'Degradation characterization of thr condition.';


--
-- Name: COLUMN condition.comments; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.condition.comments IS 'Addtional information to characterize the condition.';


--
-- Name: condition_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.condition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: condition_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.condition_id_seq OWNED BY public.condition.id;


--
-- Name: node_station; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.node_station (
    id_node integer NOT NULL,
    id_station integer,
    id integer NOT NULL
);


--
-- Name: TABLE node_station; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.node_station IS 'Link between the node and the station';


--
-- Name: COLUMN node_station.id_node; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.node_station.id_node IS 'Reference (foreign key) to destiny node table.';


--
-- Name: COLUMN node_station.id_station; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.node_station.id_station IS 'Reference (foreign key) to station table.';


--
-- Name: COLUMN node_station.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.node_station.id IS 'Autoincrement primary key.';


--
-- Name: connections_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.connections_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: connections_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.connections_id_seq OWNED BY public.node_station.id;


--
-- Name: constellation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.constellation (
    id integer NOT NULL,
    identifier_1ch character varying NOT NULL,
    identifier_3ch character varying(3) NOT NULL,
    name character varying(127) NOT NULL,
    official boolean NOT NULL
);


--
-- Name: TABLE constellation; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.constellation IS 'List of known satellite navigation systems (constellations).';


--
-- Name: COLUMN constellation.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.constellation.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN constellation.identifier_1ch; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.constellation.identifier_1ch IS 'Satellite system identifier: 1-char id.';


--
-- Name: COLUMN constellation.identifier_3ch; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.constellation.identifier_3ch IS 'Satellite system identifier: 3-char id.';


--
-- Name: COLUMN constellation.name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.constellation.name IS 'Full acronym of satellite system.';


--
-- Name: COLUMN constellation.official; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.constellation.official IS 'Whether this constellation is officially recognized by the DB admin.';


--
-- Name: constellation_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.constellation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: constellation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.constellation_id_seq OWNED BY public.constellation.id;


--
-- Name: contact; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.contact (
    name text NOT NULL,
    email text,
    phone text,
    comment text,
    id_agency integer NOT NULL,
    pid text NOT NULL
);


--
-- Name: TABLE contact; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.contact IS 'List of contact persons for agency, station, network, log and items.';


--
-- Name: COLUMN contact.name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.contact.name IS 'Contact person name.';


--
-- Name: COLUMN contact.email; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.contact.email IS 'Contact person institutional email.';


--
-- Name: COLUMN contact.phone; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.contact.phone IS 'Contact person institutional phone number.';


--
-- Name: COLUMN contact.comment; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.contact.comment IS 'Additional information on contact person.';


--
-- Name: COLUMN contact.id_agency; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.contact.id_agency IS 'Reference (foreign key) to agency table to identify contact person agency.';


--
-- Name: COLUMN contact.pid; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.contact.pid IS 'persistent identifier';


--
-- Name: country; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.country (
    name text NOT NULL,
    iso_code character(3) NOT NULL
);


--
-- Name: TABLE country; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.country IS 'List of world countries by name and ISO 3166-1 alpha-3 codes.';


--
-- Name: COLUMN country.name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.country.name IS 'Country name.';


--
-- Name: COLUMN country.iso_code; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.country.iso_code IS 'Country ISO 3166-1 alpha-3 code.';


--
-- Name: data_center_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.data_center_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: data_center; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.data_center (
    id integer DEFAULT nextval('public.data_center_id_seq'::regclass) NOT NULL,
    acronym text NOT NULL,
    id_agency integer NOT NULL,
    hostname text NOT NULL,
    name text NOT NULL,
    protocol text NOT NULL,
    login_options text
);


--
-- Name: TABLE data_center; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.data_center IS 'Table to store Data center information.';


--
-- Name: COLUMN data_center.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.data_center.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN data_center.acronym; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.data_center.acronym IS 'Unique acronym used to identify data center.';


--
-- Name: COLUMN data_center.id_agency; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.data_center.id_agency IS 'Reference (foreign_key) to agency table.';


--
-- Name: COLUMN data_center.hostname; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.data_center.hostname IS 'Full DNS name of the computer serving the file (e.g.,  ftp.example.com).';


--
-- Name: COLUMN data_center.name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.data_center.name IS 'Data center name.';


--
-- Name: COLUMN data_center.protocol; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.data_center.protocol IS 'Communication protocol, most commonly ftp, http, sftp, https.';


--
-- Name: COLUMN data_center.login_options; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.data_center.login_options IS 'List of allowed accesss types.';


--
-- Name: data_center_access_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.data_center_access_types (
    id integer NOT NULL,
    id_datacenter integer,
    open_access boolean,
    url text,
    access_type text
);


--
-- Name: TABLE data_center_access_types; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.data_center_access_types IS 'Describe what types of access are allowed';


--
-- Name: data_center_structure_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.data_center_structure_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: data_center_structure; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.data_center_structure (
    id integer DEFAULT nextval('public.data_center_structure_id_seq'::regclass) NOT NULL,
    id_data_center integer NOT NULL,
    id_file_type integer NOT NULL,
    directory_naming text NOT NULL,
    comments text
);


--
-- Name: TABLE data_center_structure; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.data_center_structure IS 'Describe the repository structure depending on the file type.';


--
-- Name: COLUMN data_center_structure.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.data_center_structure.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN data_center_structure.id_data_center; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.data_center_structure.id_data_center IS 'Reference (foreign key) to data_center table.';


--
-- Name: COLUMN data_center_structure.id_file_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.data_center_structure.id_file_type IS 'Type of file.';


--
-- Name: COLUMN data_center_structure.directory_naming; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.data_center_structure.directory_naming IS 'Give the directory naming schema with the usual  WWWW  for GPS week,  DDD  for day and so on.';


--
-- Name: COLUMN data_center_structure.comments; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.data_center_structure.comments IS 'Additional information.';


--
-- Name: document; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.document (
    id integer NOT NULL,
    date date NOT NULL,
    title text NOT NULL,
    description text,
    link text NOT NULL,
    id_station integer,
    id_item integer,
    id_document_type integer NOT NULL,
    id_license integer
);


--
-- Name: TABLE document; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.document IS 'Different types of documentation on stations and items.';


--
-- Name: COLUMN document.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.document.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN document.date; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.document.date IS 'Document date.';


--
-- Name: COLUMN document.title; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.document.title IS 'Document title.';


--
-- Name: COLUMN document.description; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.document.description IS 'Document detailed description.';


--
-- Name: COLUMN document.link; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.document.link IS 'Document location (url).';


--
-- Name: COLUMN document.id_station; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.document.id_station IS 'Reference (foreign key) to station table.';


--
-- Name: COLUMN document.id_item; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.document.id_item IS 'Reference (foreign key) to item table.';


--
-- Name: COLUMN document.id_document_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.document.id_document_type IS 'Reference (foreign key) to item_type table to specify the document type.';


--
-- Name: COLUMN document.id_license; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.document.id_license IS 'Reference (foreign key) to licence table to specify the document license.';


--
-- Name: document_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.document_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: document_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.document_id_seq OWNED BY public.document.id;


--
-- Name: document_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.document_type (
    id integer NOT NULL,
    name text NOT NULL
);


--
-- Name: TABLE document_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.document_type IS 'List of document types.';


--
-- Name: COLUMN document_type.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.document_type.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN document_type.name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.document_type.name IS 'Type of document.';


--
-- Name: document_type_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.document_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: document_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.document_type_id_seq OWNED BY public.document_type.id;


--
-- Name: effects; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.effects (
    id integer NOT NULL,
    type text NOT NULL
);


--
-- Name: TABLE effects; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.effects IS 'List of effects that characterize a station condition.';


--
-- Name: COLUMN effects.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.effects.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN effects.type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.effects.type IS 'Type of effect.';


--
-- Name: effects_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.effects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: effects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.effects_id_seq OWNED BY public.effects.id;


--
-- Name: file_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.file_type (
    id integer NOT NULL,
    format text NOT NULL,
    sampling_window text,
    sampling_frequency text
);


--
-- Name: TABLE file_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.file_type IS 'Types of files acceptable as data files.';


--
-- Name: COLUMN file_type.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.file_type.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN file_type.format; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.file_type.format IS 'Acceptable formats are: RINEX2, RINEX3, RINEXN2, RINEXN3, RINEXM2, RINEXM3, QC-XML.';


--
-- Name: COLUMN file_type.sampling_window; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.file_type.sampling_window IS 'Time window of data collection: 24hour, 1hour, 15min.';


--
-- Name: COLUMN file_type.sampling_frequency; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.file_type.sampling_frequency IS 'Time between datapoints: 30s, 15s, 1s, 5Hz, 10Hz.';


--
-- Name: file_type_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.file_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: file_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.file_type_id_seq OWNED BY public.file_type.id;


--
-- Name: gnss_obsnames; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.gnss_obsnames (
    id smallint NOT NULL,
    name character varying(3) NOT NULL,
    frequency_band integer NOT NULL,
    obstype character(1) NOT NULL,
    channel character(1),
    official boolean NOT NULL
);


--
-- Name: TABLE gnss_obsnames; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.gnss_obsnames IS 'List of all the possible observations in a GNSS RINEX file.';


--
-- Name: COLUMN gnss_obsnames.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gnss_obsnames.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN gnss_obsnames.name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gnss_obsnames.name IS 'Observation name with 3 characters (as defined in RINEX 3.03 convention), or 2 char + 1 space if RINEX 2 input.';


--
-- Name: COLUMN gnss_obsnames.frequency_band; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gnss_obsnames.frequency_band IS 'Frequency-band id number as in RINEX 3.';


--
-- Name: COLUMN gnss_obsnames.obstype; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gnss_obsnames.obstype IS 'Type of observation:  C  for code (also  P  in RINEX 2),  L  for phase,  S  for signal to noise ratio, and  D  for doppler.';


--
-- Name: COLUMN gnss_obsnames.channel; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gnss_obsnames.channel IS 'Tracking channel.';


--
-- Name: COLUMN gnss_obsnames.official; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.gnss_obsnames.official IS 'Whether officially recognized combination in RINEX 2 and 3 definitions.';


--
-- Name: gnss_obsnames_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.gnss_obsnames_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: gnss_obsnames_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.gnss_obsnames_id_seq OWNED BY public.gnss_obsnames.id;


--
-- Name: highrate_report_summary; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.highrate_report_summary (
    id bigint NOT NULL,
    id_rinex_file_highrate integer,
    date_beg timestamp without time zone NOT NULL,
    date_end timestamp without time zone NOT NULL,
    data_smp double precision NOT NULL,
    file_format text NOT NULL,
    created_timestamp timestamp without time zone,
    data_numb_epo integer,
    site_id text,
    marker_name text,
    marker_numb text,
    receiver_type text,
    receiver_numb text,
    receiver_vers text,
    antenna_dome text,
    antenna_numb text,
    antenna_type text,
    software text,
    data_int text,
    position_x text,
    position_y text,
    position_z text,
    eccentricity_e text,
    eccentricity_n text,
    eccentricity_u text,
    satellite_system text
);


--
-- Name: TABLE highrate_report_summary; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.highrate_report_summary IS 'Contains summary of information contains in the RINEX header and observations total values over all constellations.';


--
-- Name: COLUMN highrate_report_summary.satellite_system; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.highrate_report_summary.satellite_system IS 'From RINEX header SYS / # / OBS TYPES first char (example: “GRE” or “GPS+GLO+GAL” to be defined)';


--
-- Name: highrate_rinex_errors; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.highrate_rinex_errors (
    id_rinex_file_highrate integer,
    id_error_type integer,
    id integer NOT NULL
);


--
-- Name: highrate_rinex_file; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.highrate_rinex_file (
    id integer NOT NULL,
    name text NOT NULL,
    id_station integer NOT NULL,
    id_data_center_structure integer NOT NULL,
    file_size integer,
    id_file_type integer NOT NULL,
    relative_path text NOT NULL,
    reference_date timestamp without time zone NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    published_date timestamp without time zone NOT NULL,
    revision_date timestamp without time zone NOT NULL,
    md5checksum text NOT NULL,
    md5uncompressed text NOT NULL,
    status smallint NOT NULL,
    rinex_version text,
    qc_publish_date timestamp without time zone,
    t3_timestamp timestamp without time zone
);


--
-- Name: highrate_rinex_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.highrate_rinex_types (
    error_type text,
    id integer NOT NULL
);


--
-- Name: instrument_collocation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.instrument_collocation (
    id integer NOT NULL,
    id_station integer NOT NULL,
    type text NOT NULL,
    status public.coloc_status,
    date_from timestamp without time zone,
    date_to timestamp without time zone,
    comment text
);


--
-- Name: TABLE instrument_collocation; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.instrument_collocation IS 'Information on station instrument collocation.';


--
-- Name: COLUMN instrument_collocation.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.instrument_collocation.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN instrument_collocation.id_station; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.instrument_collocation.id_station IS 'Reference (foreign key) from the station table.';


--
-- Name: COLUMN instrument_collocation.type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.instrument_collocation.type IS 'Acceptable types are: GPS, GLONASS, DORIS, PRARE, SLR, VLBI, TIME, etc.';


--
-- Name: COLUMN instrument_collocation.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.instrument_collocation.status IS 'Acceptable values are: PERMANENT, MOBILE, NONE.';


--
-- Name: COLUMN instrument_collocation.date_from; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.instrument_collocation.date_from IS 'Start date in timestamp format (YYYY-MM-DD hh:mm:ss).';


--
-- Name: COLUMN instrument_collocation.date_to; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.instrument_collocation.date_to IS 'End date in timestamp format (YYYY-MM-DD hh:mm:ss).';


--
-- Name: COLUMN instrument_collocation.comment; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.instrument_collocation.comment IS 'Additional information.';


--
-- Name: instrument_collocation_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.instrument_collocation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: instrument_collocation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.instrument_collocation_id_seq OWNED BY public.instrument_collocation.id;


--
-- Name: item; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.item (
    id integer NOT NULL,
    id_item_type integer NOT NULL,
    comment text
);


--
-- Name: TABLE item; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.item IS 'Characterization of an item type, owner and producer.';


--
-- Name: COLUMN item.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.item.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN item.id_item_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.item.id_item_type IS 'Reference (foreign key) from item_type table to specify te type of item.';


--
-- Name: COLUMN item.comment; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.item.comment IS 'Additional information.';


--
-- Name: item_attribute; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.item_attribute (
    id integer NOT NULL,
    id_item integer NOT NULL,
    id_attribute integer NOT NULL,
    date_from timestamp(6) without time zone DEFAULT ('now'::text)::timestamp without time zone,
    date_to timestamp(6) without time zone,
    value_varchar text,
    value_date date,
    value_numeric numeric
);


--
-- Name: TABLE item_attribute; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.item_attribute IS 'List all attributes of an item.';


--
-- Name: COLUMN item_attribute.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.item_attribute.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN item_attribute.id_item; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.item_attribute.id_item IS 'Reference (foreign key) from item table item.';


--
-- Name: COLUMN item_attribute.id_attribute; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.item_attribute.id_attribute IS 'Reference (foreign key) from item table attribute.';


--
-- Name: COLUMN item_attribute.date_from; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.item_attribute.date_from IS 'Start date in timestamp format (YYYY-MM-DD hh:mm:ss).';


--
-- Name: COLUMN item_attribute.date_to; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.item_attribute.date_to IS 'End date in timestamp format (YYYY-MM-DD hh:mm:ss).';


--
-- Name: COLUMN item_attribute.value_varchar; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.item_attribute.value_varchar IS 'Maximum value (50 characters).';


--
-- Name: COLUMN item_attribute.value_date; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.item_attribute.value_date IS 'Date format (YYYY-MM-DD).';


--
-- Name: COLUMN item_attribute.value_numeric; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.item_attribute.value_numeric IS 'Numeric value.';


--
-- Name: item_attribute_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.item_attribute_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: item_attribute_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.item_attribute_id_seq OWNED BY public.item_attribute.id;


--
-- Name: item_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.item_id_seq OWNED BY public.item.id;


--
-- Name: item_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.item_type (
    id integer NOT NULL,
    name text NOT NULL
);


--
-- Name: TABLE item_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.item_type IS 'List of different item types.';


--
-- Name: COLUMN item_type.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.item_type.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN item_type.name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.item_type.name IS 'Item name.';


--
-- Name: item_type_attribute; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.item_type_attribute (
    id integer NOT NULL,
    id_item_type integer NOT NULL,
    id_attribute integer NOT NULL
);


--
-- Name: TABLE item_type_attribute; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.item_type_attribute IS 'List of attributes of a specific item type.';


--
-- Name: COLUMN item_type_attribute.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.item_type_attribute.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN item_type_attribute.id_item_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.item_type_attribute.id_item_type IS 'Reference (foreign key) from item_type table.';


--
-- Name: COLUMN item_type_attribute.id_attribute; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.item_type_attribute.id_attribute IS 'Reference (foreign key) from attribute table.';


--
-- Name: item_type_attribute_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.item_type_attribute_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: item_type_attribute_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.item_type_attribute_id_seq OWNED BY public.item_type_attribute.id;


--
-- Name: item_type_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.item_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: item_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.item_type_id_seq OWNED BY public.item_type.id;


--
-- Name: license; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.license (
    id integer NOT NULL,
    id_station integer NOT NULL,
    begin_date date,
    end_date date,
    id_file_type integer NOT NULL,
    embargo_period text,
    license_type text,
    link text
);


--
-- Name: TABLE license; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.license IS 'License information applied to each station.';


--
-- Name: COLUMN license.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.license.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN license.id_station; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.license.id_station IS 'Identification of the station to which was granted the licence.';


--
-- Name: COLUMN license.begin_date; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.license.begin_date IS 'Licence start date.';


--
-- Name: COLUMN license.end_date; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.license.end_date IS 'Licence end date.';


--
-- Name: COLUMN license.id_file_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.license.id_file_type IS 'Types of files (RINEX2, 24h, 30s; RINEX2, 1h, 1s; RINEX3, 24h, 30s; RINEX3, 1h, 1s; QC-XML).';


--
-- Name: COLUMN license.embargo_period; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.license.embargo_period IS 'Detail on the specification of the embargo period.';


--
-- Name: COLUMN license.license_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.license.license_type IS 'Type of license applied (Creative Commons licenses: CC BY, CC BY-SA, CC BY-NC, CC BY-ND, CC BY-NC-SA, CC BY-NC-ND9.';


--
-- Name: COLUMN license.link; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.license.link IS 'URL of the license documentation.';


--
-- Name: license_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.license_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: local_ties; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.local_ties (
    id integer NOT NULL,
    id_station integer NOT NULL,
    name text NOT NULL,
    usage text,
    cpd_num text,
    iers_domes text,
    dx numeric,
    dy numeric,
    dz numeric,
    accuracy numeric,
    survey_method text,
    date_at date,
    comment text
);


--
-- Name: TABLE local_ties; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.local_ties IS 'Station local ties characterization.';


--
-- Name: COLUMN local_ties.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.local_ties.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN local_ties.id_station; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.local_ties.id_station IS 'Reference (foreign key) from station table.';


--
-- Name: COLUMN local_ties.name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.local_ties.name IS 'Local tie name.';


--
-- Name: COLUMN local_ties.usage; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.local_ties.usage IS 'Local tie type of usage information.';


--
-- Name: COLUMN local_ties.cpd_num; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.local_ties.cpd_num IS 'Local tie Cape Photographic Durchmusterung number (cpd_num value).';


--
-- Name: COLUMN local_ties.iers_domes; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.local_ties.iers_domes IS 'Unique International Earth Rotation and Reference Systems Service (IERS) Dome number.';


--
-- Name: COLUMN local_ties.dx; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.local_ties.dx IS 'DX differential Components from GNSS Marker to the tied monument (ITRS) in (m).';


--
-- Name: COLUMN local_ties.dy; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.local_ties.dy IS 'DY differential Components from GNSS Marker to the tied monument (ITRS) in (m)';


--
-- Name: COLUMN local_ties.dz; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.local_ties.dz IS 'DZ differential Components from GNSS Marker to the tied monument (ITRS) in (m).';


--
-- Name: COLUMN local_ties.accuracy; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.local_ties.accuracy IS 'Accuracy values in millimeters.';


--
-- Name: COLUMN local_ties.survey_method; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.local_ties.survey_method IS 'Survey method description.';


--
-- Name: COLUMN local_ties.date_at; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.local_ties.date_at IS 'Date of measurement for the local tie in date format (YYYY-MM-DD).';


--
-- Name: COLUMN local_ties.comment; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.local_ties.comment IS 'Additional comments.';


--
-- Name: local_ties_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.local_ties_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: local_ties_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.local_ties_id_seq OWNED BY public.local_ties.id;


--
-- Name: log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.log (
    id integer NOT NULL,
    title character varying(50) NOT NULL,
    date date,
    id_log_type integer NOT NULL,
    id_station integer NOT NULL,
    modified text,
    previous character varying(30),
    id_contact integer,
    change_date timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- Name: TABLE log; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.log IS 'Detail log information for a station including addtional information like contact person and log type from in obtained from tables contact and log_type.';


--
-- Name: COLUMN log.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.log.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN log.title; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.log.title IS 'Log title name.';


--
-- Name: COLUMN log.date; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.log.date IS 'Log date format (YYYY-MM-DD).';


--
-- Name: COLUMN log.id_log_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.log.id_log_type IS 'Reference (foreign key) from log_type table.';


--
-- Name: COLUMN log.id_station; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.log.id_station IS 'Reference (foreign key) from station table.';


--
-- Name: COLUMN log.modified; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.log.modified IS 'Modified information.';


--
-- Name: COLUMN log.previous; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.log.previous IS 'Previous site log file information.';


--
-- Name: COLUMN log.id_contact; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.log.id_contact IS 'Person who has modified or created the site log.';


--
-- Name: COLUMN log.change_date; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.log.change_date IS 'Timestamp of the log insertion or update.';


--
-- Name: log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.log_id_seq OWNED BY public.log.id;


--
-- Name: log_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.log_type (
    id integer NOT NULL,
    name character varying(30) NOT NULL
);


--
-- Name: TABLE log_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.log_type IS '~List of log types.';


--
-- Name: COLUMN log_type.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.log_type.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN log_type.name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.log_type.name IS 'Type log name.';


--
-- Name: log_type_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.log_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: log_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.log_type_id_seq OWNED BY public.log_type.id;


--
-- Name: network; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.network (
    id integer NOT NULL,
    name text NOT NULL,
    full_name text,
    link text,
    network_type text,
    doi text,
    pid text
);


--
-- Name: TABLE network; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.network IS 'List of network names and associated contact person responsible for the network.';


--
-- Name: COLUMN network.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.network.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN network.name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.network.name IS 'Unique network name.';


--
-- Name: COLUMN network.full_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.network.full_name IS 'Full name of the network e.g.: European Permanent Network.';


--
-- Name: COLUMN network.link; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.network.link IS 'Network web address.';


--
-- Name: COLUMN network.network_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.network.network_type IS 'Type of network e.g.: Local, International or Virtual.';


--
-- Name: COLUMN network.doi; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.network.doi IS 'URL of network doi landing page.';


--
-- Name: network_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.network_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: network_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.network_id_seq OWNED BY public.network.id;


--
-- Name: network_type_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.network_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: node; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.node (
    name text NOT NULL,
    host text NOT NULL,
    port text NOT NULL,
    dbname text NOT NULL,
    username text NOT NULL,
    password text NOT NULL,
    id integer NOT NULL,
    url text,
    email text,
    contact_name text NOT NULL,
    api_url text,
    web_client_url text
);


--
-- Name: TABLE node; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.node IS 'List of existing nodes.';


--
-- Name: COLUMN node.name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.node.name IS 'Node name.';


--
-- Name: COLUMN node.host; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.node.host IS 'Host in which the node was deployed.';


--
-- Name: COLUMN node.port; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.node.port IS 'Node port.';


--
-- Name: COLUMN node.dbname; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.node.dbname IS 'Name of the database deployed at the node.';


--
-- Name: COLUMN node.username; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.node.username IS 'Node database username.';


--
-- Name: COLUMN node.password; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.node.password IS 'Node database password.';


--
-- Name: COLUMN node.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.node.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN node.url; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.node.url IS 'Node url.';


--
-- Name: COLUMN node.email; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.node.email IS 'Node contact person email.';


--
-- Name: COLUMN node.contact_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.node.contact_name IS 'Node contact person name.';


--
-- Name: node_data_center; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.node_data_center (
    id_node integer NOT NULL,
    id_data_center integer NOT NULL,
    id integer NOT NULL
);


--
-- Name: TABLE node_data_center; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.node_data_center IS 'List to show in which nodes a data_center for a station would be deployed.';


--
-- Name: COLUMN node_data_center.id_node; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.node_data_center.id_node IS 'Reference (foreign_key) from node table.';


--
-- Name: COLUMN node_data_center.id_data_center; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.node_data_center.id_data_center IS 'Reference (foreign_key) from data_center table.';


--
-- Name: COLUMN node_data_center.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.node_data_center.id IS 'Autoincrement primary key.';


--
-- Name: node_data_center_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.node_data_center_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: node_data_center_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.node_data_center_id_seq OWNED BY public.node_data_center.id;


--
-- Name: node_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.node_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: node_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.node_id_seq OWNED BY public.node.id;


--
-- Name: noise_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.noise_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: qc_constellation_summary; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qc_constellation_summary (
    id integer NOT NULL,
    id_qc_report_summary integer NOT NULL,
    id_constellation integer NOT NULL,
    nsat integer NOT NULL,
    xele integer NOT NULL,
    epo_expt integer NOT NULL,
    epo_have integer NOT NULL,
    epo_usbl integer NOT NULL,
    xcod_epo integer,
    xcod_sat integer,
    xpha_epo integer NOT NULL,
    xpha_sat integer NOT NULL,
    xint_epo integer NOT NULL,
    xint_sat integer NOT NULL,
    xint_sig integer NOT NULL,
    xint_slp integer NOT NULL,
    x_crd numeric(35,15),
    y_crd numeric(35,15),
    z_crd numeric(35,15),
    x_rms numeric(35,15),
    y_rms numeric(35,15),
    z_rms numeric(35,15)
);


--
-- Name: TABLE qc_constellation_summary; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.qc_constellation_summary IS 'Contains a summary of the main metrics depending on the constellation.';


--
-- Name: COLUMN qc_constellation_summary.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_constellation_summary.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN qc_constellation_summary.id_qc_report_summary; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_constellation_summary.id_qc_report_summary IS 'Reference (foreign key) from qc_report_summary table.';


--
-- Name: COLUMN qc_constellation_summary.id_constellation; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_constellation_summary.id_constellation IS 'Reference (foreign key) from constellation table.';


--
-- Name: COLUMN qc_constellation_summary.nsat; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_constellation_summary.nsat IS 'Total number of satellites.';


--
-- Name: COLUMN qc_constellation_summary.xele; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_constellation_summary.xele IS 'Satellites/epochs without elevation data.';


--
-- Name: COLUMN qc_constellation_summary.epo_expt; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_constellation_summary.epo_expt IS 'Estimated number of expected data epochs.';


--
-- Name: COLUMN qc_constellation_summary.epo_have; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_constellation_summary.epo_have IS 'Estimated number of present data epochs.';


--
-- Name: COLUMN qc_constellation_summary.epo_usbl; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_constellation_summary.epo_usbl IS 'Estimated number of usable data epochs.';


--
-- Name: COLUMN qc_constellation_summary.xcod_epo; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_constellation_summary.xcod_epo IS 'Count of epochs with single-freq GNSS code only.';


--
-- Name: COLUMN qc_constellation_summary.xcod_sat; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_constellation_summary.xcod_sat IS 'Count of epochs/sats with single-freq GNSS code only.';


--
-- Name: COLUMN qc_constellation_summary.xpha_epo; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_constellation_summary.xpha_epo IS 'Count of epochs with single-freq GNSS phase only.';


--
-- Name: COLUMN qc_constellation_summary.xpha_sat; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_constellation_summary.xpha_sat IS 'Count of epochs/sats with single-freq GNSS phase only.';


--
-- Name: COLUMN qc_constellation_summary.xint_epo; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_constellation_summary.xint_epo IS 'Count of phase interruptions due to gap in epochs.';


--
-- Name: COLUMN qc_constellation_summary.xint_sat; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_constellation_summary.xint_sat IS 'Count of phase interruptions due to gap in satellites.';


--
-- Name: COLUMN qc_constellation_summary.xint_sig; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_constellation_summary.xint_sig IS 'Count of phase interruptions due to gap in signals.';


--
-- Name: COLUMN qc_constellation_summary.xint_slp; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_constellation_summary.xint_slp IS 'Count of phase interruptions due to cycle-slip.';


--
-- Name: COLUMN qc_constellation_summary.x_crd; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_constellation_summary.x_crd IS 'X_crd value.';


--
-- Name: COLUMN qc_constellation_summary.y_crd; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_constellation_summary.y_crd IS 'Y_crd value.';


--
-- Name: COLUMN qc_constellation_summary.z_crd; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_constellation_summary.z_crd IS 'Z_crd value.';


--
-- Name: COLUMN qc_constellation_summary.x_rms; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_constellation_summary.x_rms IS 'X_rms.';


--
-- Name: COLUMN qc_constellation_summary.y_rms; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_constellation_summary.y_rms IS 'Y_rms.';


--
-- Name: COLUMN qc_constellation_summary.z_rms; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_constellation_summary.z_rms IS 'Z_rms.';


--
-- Name: qc_constellation_summary_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.qc_constellation_summary_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: qc_constellation_summary_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.qc_constellation_summary_id_seq OWNED BY public.qc_constellation_summary.id;


--
-- Name: qc_navigation_msg; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qc_navigation_msg (
    id integer NOT NULL,
    id_qc_report_summary integer NOT NULL,
    id_constellation integer NOT NULL,
    nsat integer NOT NULL,
    have integer NOT NULL
);


--
-- Name: TABLE qc_navigation_msg; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.qc_navigation_msg IS 'Navigation message numbers for each constellation.';


--
-- Name: COLUMN qc_navigation_msg.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_navigation_msg.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN qc_navigation_msg.id_qc_report_summary; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_navigation_msg.id_qc_report_summary IS 'Reference (foreign key) from qc_report_summary table.';


--
-- Name: COLUMN qc_navigation_msg.id_constellation; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_navigation_msg.id_constellation IS 'Satellite system.';


--
-- Name: COLUMN qc_navigation_msg.nsat; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_navigation_msg.nsat IS 'Number of satellites.';


--
-- Name: COLUMN qc_navigation_msg.have; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_navigation_msg.have IS 'Number of existing messages.';


--
-- Name: qc_navigation_msg_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.qc_navigation_msg_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: qc_navigation_msg_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.qc_navigation_msg_id_seq OWNED BY public.qc_navigation_msg.id;


--
-- Name: qc_observation_summary_c; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qc_observation_summary_c (
    id bigint NOT NULL,
    id_qc_constellation_summary integer NOT NULL,
    id_gnss_obsnames smallint NOT NULL,
    obs_sats smallint NOT NULL,
    obs_have integer NOT NULL,
    obs_expt integer,
    user_have integer NOT NULL,
    user_expt integer,
    cod_mpth double precision
);


--
-- Name: TABLE qc_observation_summary_c; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.qc_observation_summary_c IS 'Details about each observable of type C (pseudorange aka code) or type P (RINEX 2) of one constellation.';


--
-- Name: COLUMN qc_observation_summary_c.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_c.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN qc_observation_summary_c.id_qc_constellation_summary; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_c.id_qc_constellation_summary IS 'Link to the constellation summary for this QC file.';


--
-- Name: COLUMN qc_observation_summary_c.id_gnss_obsnames; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_c.id_gnss_obsnames IS 'Link to the observation name (3 character following RINEX 3 observable naming convention).';


--
-- Name: COLUMN qc_observation_summary_c.obs_sats; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_c.obs_sats IS 'Number of satellites tracked on this channel.';


--
-- Name: COLUMN qc_observation_summary_c.obs_have; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_c.obs_have IS 'Sum of existing observations for this signal.';


--
-- Name: COLUMN qc_observation_summary_c.obs_expt; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_c.obs_expt IS 'Sum of expected observations for this signal.';


--
-- Name: COLUMN qc_observation_summary_c.user_have; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_c.user_have IS 'Sum of existing observations for this signal above the defined elevation cut off.';


--
-- Name: COLUMN qc_observation_summary_c.user_expt; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_c.user_expt IS 'Sum of expected observations for this signal above the defined elevation cut off.';


--
-- Name: COLUMN qc_observation_summary_c.cod_mpth; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_c.cod_mpth IS 'Mean estimated code obs multipath and noise [cm].';


--
-- Name: qc_observation_summary_c_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.qc_observation_summary_c_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: qc_observation_summary_c_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.qc_observation_summary_c_id_seq OWNED BY public.qc_observation_summary_c.id;


--
-- Name: qc_observation_summary_d; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qc_observation_summary_d (
    id bigint NOT NULL,
    id_qc_constellation_summary integer NOT NULL,
    id_gnss_obsnames smallint NOT NULL,
    obs_sats smallint NOT NULL,
    obs_have integer NOT NULL,
    obs_expt integer,
    user_have integer NOT NULL,
    user_expt integer
);


--
-- Name: TABLE qc_observation_summary_d; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.qc_observation_summary_d IS 'Details about each observable of type D (doppler) of one constellation.';


--
-- Name: COLUMN qc_observation_summary_d.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_d.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN qc_observation_summary_d.id_qc_constellation_summary; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_d.id_qc_constellation_summary IS 'Link to the constellation summary for this QC file.';


--
-- Name: COLUMN qc_observation_summary_d.id_gnss_obsnames; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_d.id_gnss_obsnames IS 'Link to the observation name (3 character following RINEX 3 observable naming convention).';


--
-- Name: COLUMN qc_observation_summary_d.obs_sats; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_d.obs_sats IS 'Number of satellites tracked on this channel.';


--
-- Name: COLUMN qc_observation_summary_d.obs_have; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_d.obs_have IS 'Sum of existing observations for this signal.';


--
-- Name: COLUMN qc_observation_summary_d.obs_expt; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_d.obs_expt IS 'Sum of expected observations for this signal.';


--
-- Name: COLUMN qc_observation_summary_d.user_have; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_d.user_have IS 'Sum of existing observations for this signal above the defined elevation cut off.';


--
-- Name: COLUMN qc_observation_summary_d.user_expt; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_d.user_expt IS 'Sum of expected observations for this signal above the defined elevation cut off.';


--
-- Name: qc_observation_summary_d_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.qc_observation_summary_d_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: qc_observation_summary_d_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.qc_observation_summary_d_id_seq OWNED BY public.qc_observation_summary_d.id;


--
-- Name: qc_observation_summary_l; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qc_observation_summary_l (
    id bigint NOT NULL,
    id_qc_constellation_summary integer NOT NULL,
    id_gnss_obsnames smallint NOT NULL,
    obs_sats smallint NOT NULL,
    obs_have integer NOT NULL,
    obs_expt integer,
    user_have integer NOT NULL,
    user_expt integer,
    pha_slps integer NOT NULL
);


--
-- Name: TABLE qc_observation_summary_l; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.qc_observation_summary_l IS 'Details about each observable of type L (carrier phase) of one constellation.';


--
-- Name: COLUMN qc_observation_summary_l.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_l.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN qc_observation_summary_l.id_qc_constellation_summary; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_l.id_qc_constellation_summary IS 'Link to the constellation summary for this QC file.';


--
-- Name: COLUMN qc_observation_summary_l.id_gnss_obsnames; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_l.id_gnss_obsnames IS 'Link to the observation name (3 character following RINEX 3 observable naming convention).';


--
-- Name: COLUMN qc_observation_summary_l.obs_sats; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_l.obs_sats IS 'Number of satellites tracked on this channel.';


--
-- Name: COLUMN qc_observation_summary_l.obs_have; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_l.obs_have IS 'Sum of existing observations for this signal.';


--
-- Name: COLUMN qc_observation_summary_l.obs_expt; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_l.obs_expt IS 'Sum of expected observations for this signal.';


--
-- Name: COLUMN qc_observation_summary_l.user_have; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_l.user_have IS 'Sum of existing observations for this signal above the defined elevation cut off.';


--
-- Name: COLUMN qc_observation_summary_l.user_expt; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_l.user_expt IS 'Sum of expected observations for this signal above the defined elevation cut off.';


--
-- Name: COLUMN qc_observation_summary_l.pha_slps; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_l.pha_slps IS 'Counts of phase obs cycle slips.';


--
-- Name: qc_observation_summary_l_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.qc_observation_summary_l_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: qc_observation_summary_l_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.qc_observation_summary_l_id_seq OWNED BY public.qc_observation_summary_l.id;


--
-- Name: qc_observation_summary_s; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qc_observation_summary_s (
    id bigint NOT NULL,
    id_qc_constellation_summary integer NOT NULL,
    id_gnss_obsnames smallint NOT NULL,
    obs_sats smallint NOT NULL,
    obs_have integer NOT NULL,
    obs_expt integer,
    user_have integer NOT NULL,
    user_expt integer
);


--
-- Name: TABLE qc_observation_summary_s; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.qc_observation_summary_s IS 'Details about each observable of type S (signal strength) of one constellation.';


--
-- Name: COLUMN qc_observation_summary_s.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_s.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN qc_observation_summary_s.id_qc_constellation_summary; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_s.id_qc_constellation_summary IS 'Link to the constellation summary for this QC file.';


--
-- Name: COLUMN qc_observation_summary_s.id_gnss_obsnames; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_s.id_gnss_obsnames IS 'Link to the observation name (3 character following RINEX 3 observable naming convention).';


--
-- Name: COLUMN qc_observation_summary_s.obs_sats; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_s.obs_sats IS 'Number of satellites tracked on this channel.';


--
-- Name: COLUMN qc_observation_summary_s.obs_have; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_s.obs_have IS 'Sum of existing observations for this signal.';


--
-- Name: COLUMN qc_observation_summary_s.obs_expt; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_s.obs_expt IS 'Sum of expected observations for this signal.';


--
-- Name: COLUMN qc_observation_summary_s.user_have; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_s.user_have IS 'Sum of existing observations for this signal above the defined elevation cut off.';


--
-- Name: COLUMN qc_observation_summary_s.user_expt; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_observation_summary_s.user_expt IS 'Sum of expected observations for this signal above the defined elevation cut off.';


--
-- Name: qc_observation_summary_s_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.qc_observation_summary_s_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: qc_observation_summary_s_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.qc_observation_summary_s_id_seq OWNED BY public.qc_observation_summary_s.id;


--
-- Name: qc_parameters; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qc_parameters (
    id integer NOT NULL,
    software_name text NOT NULL,
    software_version text NOT NULL,
    elevation_cutoff integer NOT NULL,
    gps boolean NOT NULL,
    glo boolean NOT NULL,
    gal boolean NOT NULL,
    bds boolean NOT NULL,
    qzs boolean NOT NULL,
    sbs boolean NOT NULL,
    nav_gps boolean NOT NULL,
    nav_glo boolean NOT NULL,
    nav_gal boolean NOT NULL,
    nav_bds boolean NOT NULL,
    nav_qzs boolean NOT NULL,
    nav_sbs boolean NOT NULL,
    irn boolean,
    nav_irn boolean NOT NULL,
    fwss_version text
);


--
-- Name: TABLE qc_parameters; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.qc_parameters IS 'Store the parameters with which the QC has been done.';


--
-- Name: COLUMN qc_parameters.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_parameters.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN qc_parameters.software_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_parameters.software_name IS 'Name of the QC analysis software used.';


--
-- Name: COLUMN qc_parameters.software_version; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_parameters.software_version IS 'QC analysis software version number.';


--
-- Name: COLUMN qc_parameters.elevation_cutoff; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_parameters.elevation_cutoff IS 'User defined elevation cut off.';


--
-- Name: COLUMN qc_parameters.gps; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_parameters.gps IS 'True if GPS is expected.';


--
-- Name: COLUMN qc_parameters.glo; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_parameters.glo IS 'True if GLONASS is expected.';


--
-- Name: COLUMN qc_parameters.gal; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_parameters.gal IS 'True if GALILEO is expected.';


--
-- Name: COLUMN qc_parameters.bds; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_parameters.bds IS 'True if BEIDOU is expected.';


--
-- Name: COLUMN qc_parameters.qzs; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_parameters.qzs IS 'True if QZSS is expected.';


--
-- Name: COLUMN qc_parameters.sbs; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_parameters.sbs IS 'True if SBAS is expected.';


--
-- Name: COLUMN qc_parameters.nav_gps; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_parameters.nav_gps IS 'True if there is GPS in the navigation file used.';


--
-- Name: COLUMN qc_parameters.nav_glo; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_parameters.nav_glo IS 'True if there is GLONASS in the navigation file used.';


--
-- Name: COLUMN qc_parameters.nav_gal; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_parameters.nav_gal IS 'True if there is GALILEO in the navigation file used.';


--
-- Name: COLUMN qc_parameters.nav_bds; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_parameters.nav_bds IS 'True if there is BEIDOU in the navigation file used.';


--
-- Name: COLUMN qc_parameters.nav_qzs; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_parameters.nav_qzs IS 'True if there is QZSS in the navigation file used.';


--
-- Name: COLUMN qc_parameters.nav_sbs; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_parameters.nav_sbs IS 'True if there is SBAS in the navigation file used.';


--
-- Name: COLUMN qc_parameters.irn; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_parameters.irn IS 'True if IRNSS is expected.';


--
-- Name: COLUMN qc_parameters.nav_irn; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_parameters.nav_irn IS 'True if there is IRNSS in the navigation file used.';


--
-- Name: COLUMN qc_parameters.fwss_version; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_parameters.fwss_version IS 'True if there is IRNSS in the navigation file used.';


--
-- Name: qc_parameters_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.qc_parameters_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: qc_parameters_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.qc_parameters_id_seq OWNED BY public.qc_parameters.id;


--
-- Name: qc_report_summary; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qc_report_summary (
    id integer NOT NULL,
    id_rinexfile integer,
    id_qc_parameters integer NOT NULL,
    date_beg timestamp without time zone NOT NULL,
    date_end timestamp without time zone NOT NULL,
    data_smp double precision NOT NULL,
    obs_elev real NOT NULL,
    obs_have integer NOT NULL,
    obs_expt integer,
    user_have integer NOT NULL,
    user_expt integer,
    cyc_slps integer NOT NULL,
    clk_jmps integer,
    xbeg integer NOT NULL,
    xend integer NOT NULL,
    xint integer NOT NULL,
    xsys integer NOT NULL,
    file_format text NOT NULL,
    created_timestamp timestamp without time zone,
    data_numb_epo integer,
    site_id text,
    marker_name text,
    marker_numb text,
    receiver_type text,
    receiver_numb text,
    receiver_vers text,
    antenna_dome text,
    antenna_numb text,
    antenna_type text,
    software text,
    data_int text,
    position_x text,
    position_y text,
    position_z text,
    eccentricity_e text,
    eccentricity_n text,
    eccentricity_u text
);


--
-- Name: TABLE qc_report_summary; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.qc_report_summary IS 'Contains summary for the QC file, total values over all constellations.';


--
-- Name: COLUMN qc_report_summary.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN qc_report_summary.id_rinexfile; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.id_rinexfile IS 'Reference (foreign key) from rinex_file table.';


--
-- Name: COLUMN qc_report_summary.id_qc_parameters; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.id_qc_parameters IS 'Reference (foreign key) from qc_parameters table.';


--
-- Name: COLUMN qc_report_summary.date_beg; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.date_beg IS 'The first time epoch of data from any GNSS system, date timestamp format (YYYY-MM-DD hh:mm:ss).';


--
-- Name: COLUMN qc_report_summary.date_end; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.date_end IS 'The last time epoch of data from any GNSS system, date timestamp format (YYYY-MM-DD hh:mm:ss).';


--
-- Name: COLUMN qc_report_summary.data_smp; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.data_smp IS 'Estimated data sampling interval [s].';


--
-- Name: COLUMN qc_report_summary.obs_elev; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.obs_elev IS 'Minimum observation elevation angle.';


--
-- Name: COLUMN qc_report_summary.obs_have; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.obs_have IS 'Sum of existing phase obs (all GNSS, band/frequencies).';


--
-- Name: COLUMN qc_report_summary.obs_expt; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.obs_expt IS 'Sum of expected phase obs (all GNSS, band/frequencies).';


--
-- Name: COLUMN qc_report_summary.user_have; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.user_have IS 'User_have value.';


--
-- Name: COLUMN qc_report_summary.user_expt; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.user_expt IS 'User_expt value.';


--
-- Name: COLUMN qc_report_summary.cyc_slps; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.cyc_slps IS 'Count of phase obs cycle-slips.';


--
-- Name: COLUMN qc_report_summary.clk_jmps; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.clk_jmps IS 'Count of receiver clock jumps.';


--
-- Name: COLUMN qc_report_summary.xbeg; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.xbeg IS 'Excluded epochs before begin.';


--
-- Name: COLUMN qc_report_summary.xend; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.xend IS 'Excluded epochs after nominal end.';


--
-- Name: COLUMN qc_report_summary.xint; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.xint IS 'Excluded epochs in nominal sampling.';


--
-- Name: COLUMN qc_report_summary.xsys; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.xsys IS 'Excluded data from extra systems.';


--
-- Name: COLUMN qc_report_summary.file_format; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.file_format IS 'File_format from RINEX, information comes from <head>/<file_format> in Anubis xml output.';


--
-- Name: COLUMN qc_report_summary.created_timestamp; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.created_timestamp IS '<meta><created> in Anubis xml output.';


--
-- Name: COLUMN qc_report_summary.data_numb_epo; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.data_numb_epo IS 'Number of epochs in the file.';


--
-- Name: COLUMN qc_report_summary.site_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.site_id IS 'Site name taken from <head><site_id> in Anubis xml output';


--
-- Name: COLUMN qc_report_summary.marker_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.marker_name IS 'Marker name taken from <head><marker_name> in Anubis xml output';


--
-- Name: COLUMN qc_report_summary.marker_numb; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.marker_numb IS 'Marker number taken from <head><marker_number> in Anubis xml output';


--
-- Name: COLUMN qc_report_summary.receiver_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.receiver_type IS 'Receiver type taken from <head><receiver_type> in Anubis xml output';


--
-- Name: COLUMN qc_report_summary.receiver_numb; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.receiver_numb IS 'Receiver number taken from <head><receiver_numb> in Anubis xml output';


--
-- Name: COLUMN qc_report_summary.receiver_vers; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.receiver_vers IS 'Receiver version taken from <head><receiver_vers> in Anubis xml output.';


--
-- Name: COLUMN qc_report_summary.antenna_dome; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.antenna_dome IS 'Antenna radome taken from <head><antenna_dome> in Anubis xml output';


--
-- Name: COLUMN qc_report_summary.antenna_numb; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.antenna_numb IS 'Antenna serial number   taken from <head><antenna_numb> in Anubis xml output';


--
-- Name: COLUMN qc_report_summary.antenna_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.antenna_type IS 'Antenna type taken from <head><antenna_type> in Anubis xml output';


--
-- Name: COLUMN qc_report_summary.software; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.software IS 'Software taken from <head><software> in Anubis xml output.';


--
-- Name: COLUMN qc_report_summary.data_int; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.data_int IS 'data interval taken from <head><data_int> in Anubis xml output';


--
-- Name: COLUMN qc_report_summary.position_x; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.position_x IS ' Position_X taken from <head><position><coordinate><gml:Point><gml:pos>[0] in Anubis xml output';


--
-- Name: COLUMN qc_report_summary.position_y; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.position_y IS 'Position_Y taken from <head><position><coordinate><gml:Point><gml:pos>[1] in Anubis xml output';


--
-- Name: COLUMN qc_report_summary.position_z; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.position_z IS 'Position_Z taken from <head><position><coordinate><gml:Point><gml:pos>[2] in Anubis xml output';


--
-- Name: COLUMN qc_report_summary.eccentricity_e; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.eccentricity_e IS 'East component of eccentricity taken from <head><eccentricity><axis name="E"> in Anubis xml output';


--
-- Name: COLUMN qc_report_summary.eccentricity_n; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.eccentricity_n IS 'North component of eccentricity taken from <head><eccentricity><axis name="N"> in Anubis xml output';


--
-- Name: COLUMN qc_report_summary.eccentricity_u; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.qc_report_summary.eccentricity_u IS 'Up component of eccentricity taken from <head><eccentricity><axis name="U"> in Anubis xml output';


--
-- Name: qc_report_summary_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.qc_report_summary_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: qc_report_summary_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.qc_report_summary_id_seq OWNED BY public.qc_report_summary.id;


--
-- Name: rinex_error_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.rinex_error_types (
    error_type text,
    id integer NOT NULL
);


--
-- Name: TABLE rinex_error_types; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.rinex_error_types IS 'List of rinex files errors.';


--
-- Name: COLUMN rinex_error_types.error_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.rinex_error_types.error_type IS 'Reference (foreign key) from error_type table.';


--
-- Name: COLUMN rinex_error_types.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.rinex_error_types.id IS 'Autoincrement primary key.';


--
-- Name: rinex_error_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.rinex_error_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rinex_error_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.rinex_error_types_id_seq OWNED BY public.rinex_error_types.id;


--
-- Name: rinex_errors; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.rinex_errors (
    id_rinex_file integer,
    id_error_type integer,
    id integer NOT NULL
);


--
-- Name: TABLE rinex_errors; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.rinex_errors IS 'To identify rinex files with errors.';


--
-- Name: COLUMN rinex_errors.id_rinex_file; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.rinex_errors.id_rinex_file IS 'Reference (foreign key) from rinex_file table.';


--
-- Name: COLUMN rinex_errors.id_error_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.rinex_errors.id_error_type IS 'Reference (foreign key) from error_type table.';


--
-- Name: COLUMN rinex_errors.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.rinex_errors.id IS 'Autoincrement primary key.';


--
-- Name: rinex_errors_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.rinex_errors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rinex_errors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.rinex_errors_id_seq OWNED BY public.rinex_errors.id;


--
-- Name: rinex_file; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.rinex_file (
    id integer NOT NULL,
    name text NOT NULL,
    id_station integer NOT NULL,
    id_data_center_structure integer NOT NULL,
    file_size integer,
    id_file_type integer NOT NULL,
    relative_path text NOT NULL,
    reference_date timestamp without time zone NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    published_date timestamp without time zone NOT NULL,
    revision_date timestamp without time zone NOT NULL,
    md5checksum text NOT NULL,
    md5uncompressed text NOT NULL,
    status smallint NOT NULL,
    rinex_version text,
    qc_publish_date timestamp without time zone,
    t3_timestamp timestamp without time zone
);


--
-- Name: TABLE rinex_file; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.rinex_file IS 'List of RINEX data files.';


--
-- Name: COLUMN rinex_file.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.rinex_file.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN rinex_file.name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.rinex_file.name IS 'Rinex file name.';


--
-- Name: COLUMN rinex_file.id_station; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.rinex_file.id_station IS 'Reference (foreign key) from station table.';


--
-- Name: COLUMN rinex_file.id_data_center_structure; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.rinex_file.id_data_center_structure IS 'Reference (foreign key) from data_center_structure table.';


--
-- Name: COLUMN rinex_file.file_size; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.rinex_file.file_size IS 'File size.';


--
-- Name: COLUMN rinex_file.id_file_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.rinex_file.id_file_type IS 'Reference (foreign key) from file_type table.';


--
-- Name: COLUMN rinex_file.relative_path; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.rinex_file.relative_path IS 'Relative path of the file directory.';


--
-- Name: COLUMN rinex_file.reference_date; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.rinex_file.reference_date IS 'Nominal start of data interval, format timestamp (YYYY-MM-DD hh:mm:ss).';


--
-- Name: COLUMN rinex_file.creation_date; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.rinex_file.creation_date IS 'The time when the file was created by the originating datacenter, format timestamp (YYYY-MM-DD hh:mm:ss).';


--
-- Name: COLUMN rinex_file.published_date; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.rinex_file.published_date IS 'The time when the file is made publicly available on the server, format timestamp (YYYY-MM-DD hh:mm:ss).';


--
-- Name: COLUMN rinex_file.revision_date; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.rinex_file.revision_date IS 'The time the file contents was last changed, format timestamp (YYYY-MM-DD hh:mm:ss).';


--
-- Name: COLUMN rinex_file.md5checksum; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.rinex_file.md5checksum IS 'File unique md5checksum value.';


--
-- Name: COLUMN rinex_file.md5uncompressed; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.rinex_file.md5uncompressed IS 'Md5checksum value of the uncompressed file.';


--
-- Name: COLUMN rinex_file.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.rinex_file.status IS 'Set to specific value to know if the file is available for dissemination or set to revision.';


--
-- Name: COLUMN rinex_file.rinex_version; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.rinex_file.rinex_version IS 'RINEX files version(e.g. RINEX2.11 or RINEX3.04).';


--
-- Name: COLUMN rinex_file.qc_publish_date; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.rinex_file.qc_publish_date IS 'Quality control publishing date.';


--
-- Name: COLUMN rinex_file.t3_timestamp; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.rinex_file.t3_timestamp IS 'Updated T2? Additional detail is need here';


--
-- Name: rinex_file_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.rinex_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rinex_file_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.rinex_file_id_seq OWNED BY public.rinex_file.id;


--
-- Name: role; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.role (
    contact_pid text,
    id_station text,
    role text
);


--
-- Name: seasonal_signal_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.seasonal_signal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: service; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.service (
    id integer NOT NULL,
    name text NOT NULL,
    version numeric(3,2) NOT NULL,
    comments text DEFAULT NULL::character varying,
    createdon timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updatedon timestamp without time zone,
    dbversion text
);


--
-- Name: TABLE service; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.service IS 'Table used to validate which service version should be allowed to access data from the current database';


--
-- Name: COLUMN service.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.service.id IS 'Autoincrement primary key';


--
-- Name: COLUMN service.name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.service.name IS 'Name of the service to be validated';


--
-- Name: COLUMN service.version; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.service.version IS 'The internal version of the service';


--
-- Name: COLUMN service.comments; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.service.comments IS 'Additional detail on service';


--
-- Name: COLUMN service.createdon; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.service.createdon IS 'Service creation date';


--
-- Name: COLUMN service.updatedon; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.service.updatedon IS 'Service update date';


--
-- Name: COLUMN service.dbversion; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.service.dbversion IS 'Version of the database deployed at the node (latest available from  https://gitlab.com/gpseurope/database)';


--
-- Name: service_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.service_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: service_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.service_id_seq OWNED BY public.service.id;


--
-- Name: station; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.station (
    id integer NOT NULL,
    name text NOT NULL,
    marker character(9) NOT NULL,
    description text,
    date_from timestamp without time zone,
    date_to timestamp without time zone,
    station_type text NOT NULL,
    comment text,
    iers_domes text DEFAULT NULL::character varying,
    cpd_num text DEFAULT NULL::character varying,
    country_code character(3) NOT NULL,
    doi text,
    state_or_province text,
    city text,
    location_description text,
    x numeric,
    y numeric,
    z numeric,
    lat numeric(6,4),
    lon numeric(7,4),
    elevation numeric(6,2),
    tectonic_plate text,
    monument_description text,
    inscription text,
    monument_foundation text,
    monument_height numeric,
    foudation_depth numeric,
    id_bedrock integer,
    characteristic text,
    fracture_space text,
    fracture_spacing text,
    fault_zone text,
    distance_to_fault text
);


--
-- Name: TABLE station; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.station IS 'Set of characteristics of a station and related to additional information provided by related tables.';


--
-- Name: COLUMN station.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN station.name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station.name IS 'Station name.';


--
-- Name: COLUMN station.marker; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station.marker IS 'Unique markerlongname 9 character identification marker.';


--
-- Name: COLUMN station.description; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station.description IS 'Additional information.';


--
-- Name: COLUMN station.date_from; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station.date_from IS 'Date the station was installed in timestamp format (YYYY-MM-DD hh:mm:ss).';


--
-- Name: COLUMN station.date_to; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station.date_to IS 'Date the station was removed in timestamp format (YYYY-MM-DD hh:mm:ss).';


--
-- Name: COLUMN station.station_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station.station_type IS 'Type of station.';


--
-- Name: COLUMN station.comment; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station.comment IS 'Additional information.';


--
-- Name: COLUMN station.iers_domes; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station.iers_domes IS 'Unique International Earth Rotation and Reference Systems Service (IERS) Dome number.';


--
-- Name: COLUMN station.cpd_num; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station.cpd_num IS 'Station Cape Photographic Durchmusterung number (cpd_num).';


--
-- Name: COLUMN station.country_code; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station.country_code IS 'Country code (ISO 3166-1 alpha-3 code).';


--
-- Name: COLUMN station.doi; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station.doi IS 'Station DOI';


--
-- Name: COLUMN station.monument_description; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station.monument_description IS 'Monument_description.';


--
-- Name: COLUMN station.inscription; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station.inscription IS 'Monument inscription.';


--
-- Name: COLUMN station.foudation_depth; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station.foudation_depth IS 'Monument foudation depth.';


--
-- Name: COLUMN station.characteristic; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station.characteristic IS 'Geological characteristcs.';


--
-- Name: COLUMN station.fracture_space; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station.fracture_space IS 'Geological fracture type.';


--
-- Name: COLUMN station.fracture_spacing; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station.fracture_spacing IS 'Geological fracture spacing.';


--
-- Name: COLUMN station.fault_zone; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station.fault_zone IS 'Geological fault zone.';


--
-- Name: COLUMN station.distance_to_fault; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station.distance_to_fault IS 'Geological distance to fault';


--
-- Name: station_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.station_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: station_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.station_id_seq OWNED BY public.station.id;


--
-- Name: station_item; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.station_item (
    id integer NOT NULL,
    id_station integer NOT NULL,
    id_item integer NOT NULL,
    date_from timestamp without time zone NOT NULL,
    date_to timestamp without time zone
);


--
-- Name: TABLE station_item; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.station_item IS 'List of items associated with a station.';


--
-- Name: COLUMN station_item.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station_item.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN station_item.id_station; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station_item.id_station IS 'Reference (foreign key) from station table.';


--
-- Name: COLUMN station_item.id_item; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station_item.id_item IS 'Reference (foreign key) from item table.';


--
-- Name: COLUMN station_item.date_from; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station_item.date_from IS 'Date of item instalation in timestamp format (YYYY-MM-DD hh:mm:ss).';


--
-- Name: COLUMN station_item.date_to; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station_item.date_to IS 'Date of item removal in timestamp format (YYYY-MM-DD hh:mm:ss).';


--
-- Name: station_item_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.station_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: station_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.station_item_id_seq OWNED BY public.station_item.id;


--
-- Name: station_network; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.station_network (
    id integer NOT NULL,
    id_station integer NOT NULL,
    id_network integer NOT NULL,
    include_date date
);


--
-- Name: TABLE station_network; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.station_network IS 'List of stations that belong to a network.';


--
-- Name: COLUMN station_network.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station_network.id IS 'Autoincrement primary key.';


--
-- Name: COLUMN station_network.id_station; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station_network.id_station IS 'Reference (foreign_key) from station table.';


--
-- Name: COLUMN station_network.id_network; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station_network.id_network IS 'Reference (foreign_key) from network table.';


--
-- Name: COLUMN station_network.include_date; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.station_network.include_date IS 'Date when the station was included in the network.';


--
-- Name: station_network_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.station_network_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: station_network_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.station_network_id_seq OWNED BY public.station_network.id;


--
-- Name: stations_with_files_summary; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.stations_with_files_summary AS
 SELECT s.marker,
    min(f.creation_date) AS start_date,
    max(f.creation_date) AS end_date,
    count(f.name) AS number_files_stored,
    sum(f.file_size) AS total_files_size
   FROM (public.station s
     JOIN public.rinex_file f ON ((s.id = f.id_station)))
  GROUP BY s.id, s.marker
  ORDER BY (count(f.name)) DESC
  WITH NO DATA;


--
-- Name: stations_with_highrate_files_summary; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.stations_with_highrate_files_summary AS
 SELECT s.marker,
    min(f.creation_date) AS start_date,
    max(f.creation_date) AS end_date,
    count(f.name) AS number_files_stored,
    sum(f.file_size) AS total_files_size
   FROM (public.station s
     JOIN public.highrate_rinex_file f ON ((s.id = f.id_station)))
  GROUP BY s.id, s.marker
  ORDER BY (count(f.name)) DESC
  WITH NO DATA;


--
-- Name: agency id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.agency ALTER COLUMN id SET DEFAULT nextval('public.agency_id_seq'::regclass);


--
-- Name: agency_network id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.agency_network ALTER COLUMN id SET DEFAULT nextval('public.agency_network_id_seq'::regclass);


--
-- Name: attribute id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.attribute ALTER COLUMN id SET DEFAULT nextval('public.attribute_id_seq'::regclass);


--
-- Name: bedrock id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bedrock ALTER COLUMN id SET DEFAULT nextval('public.bedrock_id_seq'::regclass);


--
-- Name: condition id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.condition ALTER COLUMN id SET DEFAULT nextval('public.condition_id_seq'::regclass);


--
-- Name: constellation id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.constellation ALTER COLUMN id SET DEFAULT nextval('public.constellation_id_seq'::regclass);


--
-- Name: document id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.document ALTER COLUMN id SET DEFAULT nextval('public.document_id_seq'::regclass);


--
-- Name: document_type id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.document_type ALTER COLUMN id SET DEFAULT nextval('public.document_type_id_seq'::regclass);


--
-- Name: effects id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.effects ALTER COLUMN id SET DEFAULT nextval('public.effects_id_seq'::regclass);


--
-- Name: file_type id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_type ALTER COLUMN id SET DEFAULT nextval('public.file_type_id_seq'::regclass);


--
-- Name: gnss_obsnames id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gnss_obsnames ALTER COLUMN id SET DEFAULT nextval('public.gnss_obsnames_id_seq'::regclass);


--
-- Name: instrument_collocation id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.instrument_collocation ALTER COLUMN id SET DEFAULT nextval('public.instrument_collocation_id_seq'::regclass);


--
-- Name: item id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.item ALTER COLUMN id SET DEFAULT nextval('public.item_id_seq'::regclass);


--
-- Name: item_attribute id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.item_attribute ALTER COLUMN id SET DEFAULT nextval('public.item_attribute_id_seq'::regclass);


--
-- Name: item_type id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.item_type ALTER COLUMN id SET DEFAULT nextval('public.item_type_id_seq'::regclass);


--
-- Name: item_type_attribute id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.item_type_attribute ALTER COLUMN id SET DEFAULT nextval('public.item_type_attribute_id_seq'::regclass);


--
-- Name: local_ties id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.local_ties ALTER COLUMN id SET DEFAULT nextval('public.local_ties_id_seq'::regclass);


--
-- Name: log id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.log ALTER COLUMN id SET DEFAULT nextval('public.log_id_seq'::regclass);


--
-- Name: log_type id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.log_type ALTER COLUMN id SET DEFAULT nextval('public.log_type_id_seq'::regclass);


--
-- Name: network id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.network ALTER COLUMN id SET DEFAULT nextval('public.network_id_seq'::regclass);


--
-- Name: node id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.node ALTER COLUMN id SET DEFAULT nextval('public.node_id_seq'::regclass);


--
-- Name: node_data_center id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.node_data_center ALTER COLUMN id SET DEFAULT nextval('public.node_data_center_id_seq'::regclass);


--
-- Name: node_station id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.node_station ALTER COLUMN id SET DEFAULT nextval('public.connections_id_seq'::regclass);


--
-- Name: qc_constellation_summary id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_constellation_summary ALTER COLUMN id SET DEFAULT nextval('public.qc_constellation_summary_id_seq'::regclass);


--
-- Name: qc_navigation_msg id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_navigation_msg ALTER COLUMN id SET DEFAULT nextval('public.qc_navigation_msg_id_seq'::regclass);


--
-- Name: qc_observation_summary_c id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_observation_summary_c ALTER COLUMN id SET DEFAULT nextval('public.qc_observation_summary_c_id_seq'::regclass);


--
-- Name: qc_observation_summary_d id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_observation_summary_d ALTER COLUMN id SET DEFAULT nextval('public.qc_observation_summary_d_id_seq'::regclass);


--
-- Name: qc_observation_summary_l id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_observation_summary_l ALTER COLUMN id SET DEFAULT nextval('public.qc_observation_summary_l_id_seq'::regclass);


--
-- Name: qc_observation_summary_s id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_observation_summary_s ALTER COLUMN id SET DEFAULT nextval('public.qc_observation_summary_s_id_seq'::regclass);


--
-- Name: qc_parameters id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_parameters ALTER COLUMN id SET DEFAULT nextval('public.qc_parameters_id_seq'::regclass);


--
-- Name: qc_report_summary id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_report_summary ALTER COLUMN id SET DEFAULT nextval('public.qc_report_summary_id_seq'::regclass);


--
-- Name: rinex_error_types id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rinex_error_types ALTER COLUMN id SET DEFAULT nextval('public.rinex_error_types_id_seq'::regclass);


--
-- Name: rinex_errors id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rinex_errors ALTER COLUMN id SET DEFAULT nextval('public.rinex_errors_id_seq'::regclass);


--
-- Name: rinex_file id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rinex_file ALTER COLUMN id SET DEFAULT nextval('public.rinex_file_id_seq'::regclass);


--
-- Name: service id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.service ALTER COLUMN id SET DEFAULT nextval('public.service_id_seq'::regclass);


--
-- Name: station id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.station ALTER COLUMN id SET DEFAULT nextval('public.station_id_seq'::regclass);


--
-- Name: station_item id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.station_item ALTER COLUMN id SET DEFAULT nextval('public.station_item_id_seq'::regclass);


--
-- Name: station_network id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.station_network ALTER COLUMN id SET DEFAULT nextval('public.station_network_id_seq'::regclass);


--
-- Data for Name: agency; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.agency (id, name, abbreviation, address, www, infos, pid) FROM stdin;
\.


--
-- Data for Name: agency_network; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.agency_network (id, id_agency, id_network) FROM stdin;
\.


--
-- Data for Name: attribute; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.attribute (id, name) FROM stdin;
1	antenna_type
2	receiver_type
3	radome_type
4	monument_type
5	serial_number
6	height
7	offset_north
8	offset_east
9	offset_elevation
10	firmware_version
11	firmware_date
12	sampling_rate
13	model
14	htcode
15	software_version
16	description
17	satellite_system
18	elevation_cutoff_setting
19	temperature_stabilization
20	antenna_reference_point
21	marker_arp_up_ecc
22	marker_arp_north_ecc
23	marker_arp_east_ecc
24	alignment_from_true_n
25	antenna_cable_type
26	antenna_cable_length
27	standard_type
28	input_frequency
29	sensor_model
30	manufacturer
31	data_sampling_interval
32	accuracy
33	aspiration
34	height_diff_to_ant
35	calibration_date
36	water_vapor_radiometer
37	distance_to_antenna
\.


--
-- Data for Name: authentication; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.authentication (id, user_id, access_token, expiration_date) FROM stdin;
\.


--
-- Data for Name: bedrock; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.bedrock (id, condition, type) FROM stdin;
\.


--
-- Data for Name: condition; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.condition (id, id_station, id_effect, date_from, date_to, degradation, comments) FROM stdin;
\.


--
-- Data for Name: constellation; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.constellation (id, identifier_1ch, identifier_3ch, name, official) FROM stdin;
1	G	GPS	GPS	t
2	R	GLO	GLONASS	t
3	S	SBS	SBAS	t
4	E	GAL	GALILEO	t
5	C	BDS	BEIDOU	t
6	J	QZS	QZSS	t
7	I	IRN	IRNSS	t
\.


--
-- Data for Name: contact; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.contact (name, email, phone, comment, id_agency, pid) FROM stdin;
\.


--
-- Data for Name: country; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.country (name, iso_code) FROM stdin;
\.


--
-- Data for Name: data_center; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.data_center (id, acronym, id_agency, hostname, name, protocol, login_options) FROM stdin;
\.


--
-- Data for Name: data_center_access_types; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.data_center_access_types (id, id_datacenter, open_access, url, access_type) FROM stdin;
\.


--
-- Data for Name: data_center_structure; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.data_center_structure (id, id_data_center, id_file_type, directory_naming, comments) FROM stdin;
\.


--
-- Data for Name: document; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.document (id, date, title, description, link, id_station, id_item, id_document_type, id_license) FROM stdin;
\.


--
-- Data for Name: document_type; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.document_type (id, name) FROM stdin;
1	picture
2	timeseries
\.


--
-- Data for Name: effects; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.effects (id, type) FROM stdin;
\.


--
-- Data for Name: file_type; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.file_type (id, format, sampling_window, sampling_frequency) FROM stdin;
1	RINEX2	24h	30s
2	RINEX2	1h	1s
3	RINEX3	24h	30s
4	RINEX3	1h	1s
5	QC-XML		
\.


--
-- Data for Name: gnss_obsnames; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.gnss_obsnames (id, name, frequency_band, obstype, channel, official) FROM stdin;
\.


--
-- Data for Name: highrate_report_summary; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.highrate_report_summary (id, id_rinex_file_highrate, date_beg, date_end, data_smp, file_format, created_timestamp, data_numb_epo, site_id, marker_name, marker_numb, receiver_type, receiver_numb, receiver_vers, antenna_dome, antenna_numb, antenna_type, software, data_int, position_x, position_y, position_z, eccentricity_e, eccentricity_n, eccentricity_u, satellite_system) FROM stdin;
\.


--
-- Data for Name: highrate_rinex_errors; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.highrate_rinex_errors (id_rinex_file_highrate, id_error_type, id) FROM stdin;
\.


--
-- Data for Name: highrate_rinex_file; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.highrate_rinex_file (id, name, id_station, id_data_center_structure, file_size, id_file_type, relative_path, reference_date, creation_date, published_date, revision_date, md5checksum, md5uncompressed, status, rinex_version, qc_publish_date, t3_timestamp) FROM stdin;
\.


--
-- Data for Name: highrate_rinex_types; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.highrate_rinex_types (error_type, id) FROM stdin;
\.


--
-- Data for Name: instrument_collocation; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.instrument_collocation (id, id_station, type, status, date_from, date_to, comment) FROM stdin;
\.


--
-- Data for Name: item; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.item (id, id_item_type, comment) FROM stdin;
\.


--
-- Data for Name: item_attribute; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.item_attribute (id, id_item, id_attribute, date_from, date_to, value_varchar, value_date, value_numeric) FROM stdin;
\.


--
-- Data for Name: item_type; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.item_type (id, name) FROM stdin;
1	antenna
2	receiver
3	radome
4	monument
5	frequency_standard
6	humidity_sensor
7	pressure_sensor
8	temperature_sensor
9	water_vapor_radiometer
10	other_instrumentation
\.


--
-- Data for Name: item_type_attribute; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.item_type_attribute (id, id_item_type, id_attribute) FROM stdin;
\.


--
-- Data for Name: license; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.license (id, id_station, begin_date, end_date, id_file_type, embargo_period, license_type, link) FROM stdin;
\.


--
-- Data for Name: local_ties; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.local_ties (id, id_station, name, usage, cpd_num, iers_domes, dx, dy, dz, accuracy, survey_method, date_at, comment) FROM stdin;
\.


--
-- Data for Name: log; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.log (id, title, date, id_log_type, id_station, modified, previous, id_contact, change_date) FROM stdin;
\.


--
-- Data for Name: log_type; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.log_type (id, name) FROM stdin;
\.


--
-- Data for Name: network; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.network (id, name, full_name, link, network_type, doi, pid) FROM stdin;
\.


--
-- Data for Name: node; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.node (name, host, port, dbname, username, password, id, url, email, contact_name, api_url, web_client_url) FROM stdin;
\.


--
-- Data for Name: node_data_center; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.node_data_center (id_node, id_data_center, id) FROM stdin;
\.


--
-- Data for Name: node_station; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.node_station (id_node, id_station, id) FROM stdin;
\.


--
-- Data for Name: qc_constellation_summary; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.qc_constellation_summary (id, id_qc_report_summary, id_constellation, nsat, xele, epo_expt, epo_have, epo_usbl, xcod_epo, xcod_sat, xpha_epo, xpha_sat, xint_epo, xint_sat, xint_sig, xint_slp, x_crd, y_crd, z_crd, x_rms, y_rms, z_rms) FROM stdin;
\.


--
-- Data for Name: qc_navigation_msg; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.qc_navigation_msg (id, id_qc_report_summary, id_constellation, nsat, have) FROM stdin;
\.


--
-- Data for Name: qc_observation_summary_c; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.qc_observation_summary_c (id, id_qc_constellation_summary, id_gnss_obsnames, obs_sats, obs_have, obs_expt, user_have, user_expt, cod_mpth) FROM stdin;
\.


--
-- Data for Name: qc_observation_summary_d; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.qc_observation_summary_d (id, id_qc_constellation_summary, id_gnss_obsnames, obs_sats, obs_have, obs_expt, user_have, user_expt) FROM stdin;
\.


--
-- Data for Name: qc_observation_summary_l; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.qc_observation_summary_l (id, id_qc_constellation_summary, id_gnss_obsnames, obs_sats, obs_have, obs_expt, user_have, user_expt, pha_slps) FROM stdin;
\.


--
-- Data for Name: qc_observation_summary_s; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.qc_observation_summary_s (id, id_qc_constellation_summary, id_gnss_obsnames, obs_sats, obs_have, obs_expt, user_have, user_expt) FROM stdin;
\.


--
-- Data for Name: qc_parameters; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.qc_parameters (id, software_name, software_version, elevation_cutoff, gps, glo, gal, bds, qzs, sbs, nav_gps, nav_glo, nav_gal, nav_bds, nav_qzs, nav_sbs, irn, nav_irn, fwss_version) FROM stdin;
\.


--
-- Data for Name: qc_report_summary; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.qc_report_summary (id, id_rinexfile, id_qc_parameters, date_beg, date_end, data_smp, obs_elev, obs_have, obs_expt, user_have, user_expt, cyc_slps, clk_jmps, xbeg, xend, xint, xsys, file_format, created_timestamp, data_numb_epo, site_id, marker_name, marker_numb, receiver_type, receiver_numb, receiver_vers, antenna_dome, antenna_numb, antenna_type, software, data_int, position_x, position_y, position_z, eccentricity_e, eccentricity_n, eccentricity_u) FROM stdin;
\.


--
-- Data for Name: rinex_error_types; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.rinex_error_types (error_type, id) FROM stdin;
\.


--
-- Data for Name: rinex_errors; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.rinex_errors (id_rinex_file, id_error_type, id) FROM stdin;
\.


--
-- Data for Name: rinex_file; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.rinex_file (id, name, id_station, id_data_center_structure, file_size, id_file_type, relative_path, reference_date, creation_date, published_date, revision_date, md5checksum, md5uncompressed, status, rinex_version, qc_publish_date, t3_timestamp) FROM stdin;
\.


--
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.role (contact_pid, id_station, role) FROM stdin;
\.


--
-- Data for Name: service; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.service (id, name, version, comments, createdon, updatedon, dbversion) FROM stdin;
1	SyncService	1.10	EPOS Synchronization Service	2019-03-29 16:42:36.564295	\N	\N
\.


--
-- Data for Name: station; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.station (id, name, marker, description, date_from, date_to, station_type, comment, iers_domes, cpd_num, country_code, doi, state_or_province, city, location_description, x, y, z, lat, lon, elevation, tectonic_plate, monument_description, inscription, monument_foundation, monument_height, foudation_depth, id_bedrock, characteristic, fracture_space, fracture_spacing, fault_zone, distance_to_fault) FROM stdin;
\.


--
-- Data for Name: station_item; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.station_item (id, id_station, id_item, date_from, date_to) FROM stdin;
\.


--
-- Data for Name: station_network; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.station_network (id, id_station, id_network, include_date) FROM stdin;
\.


--
-- Name: agency_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.agency_id_seq', 1, false);


--
-- Name: agency_network_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.agency_network_id_seq', 1, false);


--
-- Name: attribute_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.attribute_id_seq', 37, true);


--
-- Name: authentication_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.authentication_id_seq', 1, false);


--
-- Name: bedrock_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bedrock_id_seq', 1, false);


--
-- Name: condition_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.condition_id_seq', 1, false);


--
-- Name: connections_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.connections_id_seq', 1, false);


--
-- Name: constellation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.constellation_id_seq', 7, true);


--
-- Name: data_center_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.data_center_id_seq', 1, false);


--
-- Name: data_center_structure_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.data_center_structure_id_seq', 1, false);


--
-- Name: document_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.document_id_seq', 1, false);


--
-- Name: document_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.document_type_id_seq', 2, true);


--
-- Name: effects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.effects_id_seq', 1, false);


--
-- Name: file_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.file_type_id_seq', 5, true);


--
-- Name: gnss_obsnames_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.gnss_obsnames_id_seq', 1, false);


--
-- Name: instrument_collocation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.instrument_collocation_id_seq', 1, false);


--
-- Name: item_attribute_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.item_attribute_id_seq', 1, false);


--
-- Name: item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.item_id_seq', 1, false);


--
-- Name: item_type_attribute_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.item_type_attribute_id_seq', 1, false);


--
-- Name: item_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.item_type_id_seq', 10, true);


--
-- Name: license_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.license_id_seq', 1, false);


--
-- Name: local_ties_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.local_ties_id_seq', 1, false);


--
-- Name: log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.log_id_seq', 1, false);


--
-- Name: log_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.log_type_id_seq', 1, false);


--
-- Name: network_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.network_id_seq', 1, false);


--
-- Name: network_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.network_type_id_seq', 1, false);


--
-- Name: node_data_center_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.node_data_center_id_seq', 1, false);


--
-- Name: node_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.node_id_seq', 1, false);


--
-- Name: noise_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.noise_id_seq', 1, false);


--
-- Name: qc_constellation_summary_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.qc_constellation_summary_id_seq', 1, false);


--
-- Name: qc_navigation_msg_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.qc_navigation_msg_id_seq', 1, false);


--
-- Name: qc_observation_summary_c_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.qc_observation_summary_c_id_seq', 1, false);


--
-- Name: qc_observation_summary_d_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.qc_observation_summary_d_id_seq', 1, false);


--
-- Name: qc_observation_summary_l_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.qc_observation_summary_l_id_seq', 1, false);


--
-- Name: qc_observation_summary_s_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.qc_observation_summary_s_id_seq', 1, false);


--
-- Name: qc_parameters_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.qc_parameters_id_seq', 1, false);


--
-- Name: qc_report_summary_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.qc_report_summary_id_seq', 1, false);


--
-- Name: rinex_error_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.rinex_error_types_id_seq', 1, false);


--
-- Name: rinex_errors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.rinex_errors_id_seq', 1, false);


--
-- Name: rinex_file_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.rinex_file_id_seq', 1, false);


--
-- Name: seasonal_signal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.seasonal_signal_id_seq', 1, false);


--
-- Name: service_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.service_id_seq', 1, true);


--
-- Name: station_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.station_id_seq', 1, false);


--
-- Name: station_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.station_item_id_seq', 1, false);


--
-- Name: station_network_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.station_network_id_seq', 1, false);


--
-- Name: agency pk_agency; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.agency
    ADD CONSTRAINT pk_agency PRIMARY KEY (id);


--
-- Name: agency_network pk_agency_network_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.agency_network
    ADD CONSTRAINT pk_agency_network_id PRIMARY KEY (id);


--
-- Name: attribute pk_attributes; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.attribute
    ADD CONSTRAINT pk_attributes PRIMARY KEY (id);


--
-- Name: authentication pk_authentication_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.authentication
    ADD CONSTRAINT pk_authentication_id PRIMARY KEY (id);


--
-- Name: bedrock pk_bedrock; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bedrock
    ADD CONSTRAINT pk_bedrock PRIMARY KEY (id);


--
-- Name: instrument_collocation pk_collocation_instrument; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.instrument_collocation
    ADD CONSTRAINT pk_collocation_instrument PRIMARY KEY (id);


--
-- Name: condition pk_condition; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.condition
    ADD CONSTRAINT pk_condition PRIMARY KEY (id);


--
-- Name: node_station pk_connections; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.node_station
    ADD CONSTRAINT pk_connections PRIMARY KEY (id);


--
-- Name: constellation pk_constellation; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.constellation
    ADD CONSTRAINT pk_constellation PRIMARY KEY (id);


--
-- Name: contact pk_contact; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contact
    ADD CONSTRAINT pk_contact PRIMARY KEY (pid);


--
-- Name: country pk_country; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.country
    ADD CONSTRAINT pk_country PRIMARY KEY (iso_code);


--
-- Name: data_center pk_data_center; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.data_center
    ADD CONSTRAINT pk_data_center PRIMARY KEY (id);


--
-- Name: data_center_access_types pk_data_center_access_types; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.data_center_access_types
    ADD CONSTRAINT pk_data_center_access_types PRIMARY KEY (id);


--
-- Name: data_center_structure pk_dc_structure; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.data_center_structure
    ADD CONSTRAINT pk_dc_structure PRIMARY KEY (id);


--
-- Name: document_type pk_document_types; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.document_type
    ADD CONSTRAINT pk_document_types PRIMARY KEY (id);


--
-- Name: document pk_documents; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.document
    ADD CONSTRAINT pk_documents PRIMARY KEY (id);


--
-- Name: effects pk_effects; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.effects
    ADD CONSTRAINT pk_effects PRIMARY KEY (id);


--
-- Name: file_type pk_file_type; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.file_type
    ADD CONSTRAINT pk_file_type PRIMARY KEY (id);


--
-- Name: rinex_file pk_files; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rinex_file
    ADD CONSTRAINT pk_files PRIMARY KEY (id);


--
-- Name: gnss_obsnames pk_gnss_obsnames; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gnss_obsnames
    ADD CONSTRAINT pk_gnss_obsnames PRIMARY KEY (id);


--
-- Name: highrate_report_summary pk_highrate_report_summary; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.highrate_report_summary
    ADD CONSTRAINT pk_highrate_report_summary PRIMARY KEY (id);


--
-- Name: item_attribute pk_item_attribute; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.item_attribute
    ADD CONSTRAINT pk_item_attribute PRIMARY KEY (id);


--
-- Name: item_type_attribute pk_item_type_attribute; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.item_type_attribute
    ADD CONSTRAINT pk_item_type_attribute PRIMARY KEY (id);


--
-- Name: item_type pk_item_types; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.item_type
    ADD CONSTRAINT pk_item_types PRIMARY KEY (id);


--
-- Name: item pk_items; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.item
    ADD CONSTRAINT pk_items PRIMARY KEY (id);


--
-- Name: license pk_licence; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.license
    ADD CONSTRAINT pk_licence PRIMARY KEY (id);


--
-- Name: local_ties pk_local_ties; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.local_ties
    ADD CONSTRAINT pk_local_ties PRIMARY KEY (id);


--
-- Name: log_type pk_log_types; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.log_type
    ADD CONSTRAINT pk_log_types PRIMARY KEY (id);


--
-- Name: log pk_logs; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.log
    ADD CONSTRAINT pk_logs PRIMARY KEY (id);


--
-- Name: network pk_networks; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.network
    ADD CONSTRAINT pk_networks PRIMARY KEY (id);


--
-- Name: node pk_node; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.node
    ADD CONSTRAINT pk_node PRIMARY KEY (id);


--
-- Name: node_data_center pk_node_data_center; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.node_data_center
    ADD CONSTRAINT pk_node_data_center PRIMARY KEY (id);


--
-- Name: qc_navigation_msg pk_qc_navigation_msg_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_navigation_msg
    ADD CONSTRAINT pk_qc_navigation_msg_id PRIMARY KEY (id);


--
-- Name: qc_observation_summary_c pk_qc_observation_summary_c; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_observation_summary_c
    ADD CONSTRAINT pk_qc_observation_summary_c PRIMARY KEY (id);


--
-- Name: qc_observation_summary_d pk_qc_observation_summary_d; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_observation_summary_d
    ADD CONSTRAINT pk_qc_observation_summary_d PRIMARY KEY (id);


--
-- Name: qc_observation_summary_l pk_qc_observation_summary_l; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_observation_summary_l
    ADD CONSTRAINT pk_qc_observation_summary_l PRIMARY KEY (id);


--
-- Name: qc_observation_summary_s pk_qc_observation_summary_s; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_observation_summary_s
    ADD CONSTRAINT pk_qc_observation_summary_s PRIMARY KEY (id);


--
-- Name: qc_parameters pk_qc_parameters; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_parameters
    ADD CONSTRAINT pk_qc_parameters PRIMARY KEY (id);


--
-- Name: qc_report_summary pk_qc_summary; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_report_summary
    ADD CONSTRAINT pk_qc_summary PRIMARY KEY (id);


--
-- Name: qc_constellation_summary pk_qc_summary_0; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_constellation_summary
    ADD CONSTRAINT pk_qc_summary_0 PRIMARY KEY (id);


--
-- Name: rinex_error_types pk_rinex_error_types; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rinex_error_types
    ADD CONSTRAINT pk_rinex_error_types PRIMARY KEY (id);


--
-- Name: rinex_errors pk_rinex_errors; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rinex_errors
    ADD CONSTRAINT pk_rinex_errors PRIMARY KEY (id);


--
-- Name: highrate_rinex_errors pk_rinex_errors_highrate; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.highrate_rinex_errors
    ADD CONSTRAINT pk_rinex_errors_highrate PRIMARY KEY (id);


--
-- Name: highrate_rinex_file pk_rinex_file_highrate; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.highrate_rinex_file
    ADD CONSTRAINT pk_rinex_file_highrate PRIMARY KEY (id);


--
-- Name: highrate_rinex_types pk_rinex_types_highrate; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.highrate_rinex_types
    ADD CONSTRAINT pk_rinex_types_highrate PRIMARY KEY (id);


--
-- Name: service pk_service; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.service
    ADD CONSTRAINT pk_service PRIMARY KEY (id);


--
-- Name: station_item pk_station_item; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.station_item
    ADD CONSTRAINT pk_station_item PRIMARY KEY (id);


--
-- Name: station_network pk_station_network; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.station_network
    ADD CONSTRAINT pk_station_network PRIMARY KEY (id);


--
-- Name: station pk_stations; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.station
    ADD CONSTRAINT pk_stations PRIMARY KEY (id);


--
-- Name: document unq_document_date; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.document
    ADD CONSTRAINT unq_document_date UNIQUE (date);


--
-- Name: agency uq_agency_abbreviation; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.agency
    ADD CONSTRAINT uq_agency_abbreviation UNIQUE (abbreviation);


--
-- Name: attribute uq_attributes_name; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.attribute
    ADD CONSTRAINT uq_attributes_name UNIQUE (name);


--
-- Name: contact uq_contacts_namemail; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contact
    ADD CONSTRAINT uq_contacts_namemail UNIQUE (name, email);


--
-- Name: data_center uq_data_center_acronym; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.data_center
    ADD CONSTRAINT uq_data_center_acronym UNIQUE (acronym);


--
-- Name: station uq_iers_domes; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.station
    ADD CONSTRAINT uq_iers_domes UNIQUE (iers_domes);


--
-- Name: log_type uq_log_types_name; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.log_type
    ADD CONSTRAINT uq_log_types_name UNIQUE (name);


--
-- Name: network uq_networks_name; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.network
    ADD CONSTRAINT uq_networks_name UNIQUE (name);


--
-- Name: node_data_center uq_node_data_center; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.node_data_center
    ADD CONSTRAINT uq_node_data_center UNIQUE (id_node, id_data_center);


--
-- Name: rinex_error_types uq_rinex_error_types_error_type; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rinex_error_types
    ADD CONSTRAINT uq_rinex_error_types_error_type UNIQUE (error_type);


--
-- Name: highrate_rinex_file uq_rinex_file_highrate; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.highrate_rinex_file
    ADD CONSTRAINT uq_rinex_file_highrate UNIQUE (md5checksum);


--
-- Name: rinex_file uq_rinex_file_md5checksum; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rinex_file
    ADD CONSTRAINT uq_rinex_file_md5checksum UNIQUE (md5checksum);


--
-- Name: idx_agency_network_id_agency; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_agency_network_id_agency ON public.agency_network USING btree (id_agency);


--
-- Name: idx_agency_network_id_network; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_agency_network_id_network ON public.agency_network USING btree (id_network);


--
-- Name: idx_connections_destiny; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_connections_destiny ON public.node_station USING btree (id_node);


--
-- Name: idx_connections_station; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_connections_station ON public.node_station USING btree (id_station);


--
-- Name: idx_data_center; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_data_center ON public.data_center USING btree (id_agency);


--
-- Name: idx_dc_structure; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_dc_structure ON public.data_center_structure USING btree (id_data_center);


--
-- Name: idx_file; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_file ON public.rinex_file USING btree (id_station);


--
-- Name: idx_file_1; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_file_1 ON public.rinex_file USING btree (id_data_center_structure);


--
-- Name: idx_item_type_attribute; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_item_type_attribute ON public.item_type_attribute USING btree (id_item_type);


--
-- Name: idx_item_type_attribute_0; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_item_type_attribute_0 ON public.item_type_attribute USING btree (id_attribute);


--
-- Name: idx_network_network_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_network_network_type ON public.network USING btree (network_type);


--
-- Name: idx_node_data_center_data_center; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_node_data_center_data_center ON public.node_data_center USING btree (id_data_center);


--
-- Name: idx_node_data_center_id_data_center; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_node_data_center_id_data_center ON public.node_data_center USING btree (id_data_center);


--
-- Name: idx_node_data_center_id_node; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_node_data_center_id_node ON public.node_data_center USING btree (id_node);


--
-- Name: idx_node_data_center_node; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_node_data_center_node ON public.node_data_center USING btree (id_node);


--
-- Name: idx_qc_constellation_summary_id_constellation; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qc_constellation_summary_id_constellation ON public.qc_constellation_summary USING btree (id_constellation);


--
-- Name: idx_qc_filesum; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qc_filesum ON public.qc_report_summary USING btree (id_rinexfile) WITH (fillfactor='90');


--
-- Name: idx_qc_filesum_0; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qc_filesum_0 ON public.qc_report_summary USING btree (id_qc_parameters) WITH (fillfactor='90');


--
-- Name: idx_qc_navigation_msg_id_constellation; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qc_navigation_msg_id_constellation ON public.qc_navigation_msg USING btree (id_constellation);


--
-- Name: idx_qc_navigation_msg_id_qc_report_summary; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qc_navigation_msg_id_qc_report_summary ON public.qc_navigation_msg USING btree (id_qc_report_summary);


--
-- Name: idx_qc_observation_summary_c_id_gnss_obsnames; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qc_observation_summary_c_id_gnss_obsnames ON public.qc_observation_summary_c USING btree (id_gnss_obsnames) WITH (fillfactor='90');


--
-- Name: idx_qc_observation_summary_c_id_qc_constellation_summary; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qc_observation_summary_c_id_qc_constellation_summary ON public.qc_observation_summary_c USING btree (id_qc_constellation_summary) WITH (fillfactor='90');


--
-- Name: idx_qc_observation_summary_d_id_gnss_obsnames; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qc_observation_summary_d_id_gnss_obsnames ON public.qc_observation_summary_d USING btree (id_gnss_obsnames) WITH (fillfactor='90');


--
-- Name: idx_qc_observation_summary_d_id_qc_constellation_summary; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qc_observation_summary_d_id_qc_constellation_summary ON public.qc_observation_summary_d USING btree (id_qc_constellation_summary) WITH (fillfactor='90');


--
-- Name: idx_qc_observation_summary_l_id_gnss_obsnames; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qc_observation_summary_l_id_gnss_obsnames ON public.qc_observation_summary_l USING btree (id_gnss_obsnames) WITH (fillfactor='90');


--
-- Name: idx_qc_observation_summary_l_id_qc_constellation_summary; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qc_observation_summary_l_id_qc_constellation_summary ON public.qc_observation_summary_l USING btree (id_qc_constellation_summary) WITH (fillfactor='90');


--
-- Name: idx_qc_observation_summary_s_id_gnss_obsnames; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qc_observation_summary_s_id_gnss_obsnames ON public.qc_observation_summary_s USING btree (id_gnss_obsnames) WITH (fillfactor='90');


--
-- Name: idx_qc_observation_summary_s_id_qc_constellation_summary; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qc_observation_summary_s_id_qc_constellation_summary ON public.qc_observation_summary_s USING btree (id_qc_constellation_summary) WITH (fillfactor='90');


--
-- Name: idx_qc_summary; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qc_summary ON public.qc_constellation_summary USING btree (id_qc_report_summary) WITH (fillfactor='90');


--
-- Name: idx_rinex_errors; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_rinex_errors ON public.rinex_errors USING btree (id_rinex_file);


--
-- Name: idx_rinex_errors_0; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_rinex_errors_0 ON public.rinex_errors USING btree (id_error_type);


--
-- Name: idx_station_item_id_item; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_station_item_id_item ON public.station_item USING btree (id_item);


--
-- Name: idx_station_item_id_station; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_station_item_id_station ON public.station_item USING btree (id_station);


--
-- Name: contact fk_agency_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contact
    ADD CONSTRAINT fk_agency_id FOREIGN KEY (id_agency) REFERENCES public.agency(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: agency_network fk_agency_network_agency; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.agency_network
    ADD CONSTRAINT fk_agency_network_agency FOREIGN KEY (id_agency) REFERENCES public.agency(id);


--
-- Name: agency_network fk_agency_network_network; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.agency_network
    ADD CONSTRAINT fk_agency_network_network FOREIGN KEY (id_network) REFERENCES public.network(id);


--
-- Name: item_attribute fk_attribute_item_attribute; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.item_attribute
    ADD CONSTRAINT fk_attribute_item_attribute FOREIGN KEY (id_attribute) REFERENCES public.attribute(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: instrument_collocation fk_collocation_instrument_stations; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.instrument_collocation
    ADD CONSTRAINT fk_collocation_instrument_stations FOREIGN KEY (id_station) REFERENCES public.station(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: node_station fk_connections_station; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.node_station
    ADD CONSTRAINT fk_connections_station FOREIGN KEY (id_station) REFERENCES public.station(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: data_center_access_types fk_data_center_access_types; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.data_center_access_types
    ADD CONSTRAINT fk_data_center_access_types FOREIGN KEY (id_datacenter) REFERENCES public.data_center(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: data_center fk_data_center_agency; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.data_center
    ADD CONSTRAINT fk_data_center_agency FOREIGN KEY (id_agency) REFERENCES public.agency(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: data_center_structure fk_dc_structure_data_center; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.data_center_structure
    ADD CONSTRAINT fk_dc_structure_data_center FOREIGN KEY (id_data_center) REFERENCES public.data_center(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: node_station fk_destiny_node; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.node_station
    ADD CONSTRAINT fk_destiny_node FOREIGN KEY (id_node) REFERENCES public.node(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: document fk_document_types_documents; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.document
    ADD CONSTRAINT fk_document_types_documents FOREIGN KEY (id_document_type) REFERENCES public.document_type(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: condition fk_effect; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.condition
    ADD CONSTRAINT fk_effect FOREIGN KEY (id_effect) REFERENCES public.effects(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: rinex_file fk_file_station; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rinex_file
    ADD CONSTRAINT fk_file_station FOREIGN KEY (id_station) REFERENCES public.station(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: CONSTRAINT fk_file_station ON rinex_file; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON CONSTRAINT fk_file_station ON public.rinex_file IS 'Link between the station and its files';


--
-- Name: highrate_report_summary fk_highrate_report_summary; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.highrate_report_summary
    ADD CONSTRAINT fk_highrate_report_summary FOREIGN KEY (id_rinex_file_highrate) REFERENCES public.highrate_rinex_file(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: license fk_id_id_file_type; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.license
    ADD CONSTRAINT fk_id_id_file_type FOREIGN KEY (id_file_type) REFERENCES public.file_type(id);


--
-- Name: license fk_id_id_station; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.license
    ADD CONSTRAINT fk_id_id_station FOREIGN KEY (id_station) REFERENCES public.station(id);


--
-- Name: document fk_id_license_document; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.document
    ADD CONSTRAINT fk_id_license_document FOREIGN KEY (id_license) REFERENCES public.license(id);


--
-- Name: item_attribute fk_item_item_attribute; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.item_attribute
    ADD CONSTRAINT fk_item_item_attribute FOREIGN KEY (id_item) REFERENCES public.item(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: item_type_attribute fk_item_type_attribute_attribute; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.item_type_attribute
    ADD CONSTRAINT fk_item_type_attribute_attribute FOREIGN KEY (id_attribute) REFERENCES public.attribute(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: item_type_attribute fk_item_type_attribute_item_type; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.item_type_attribute
    ADD CONSTRAINT fk_item_type_attribute_item_type FOREIGN KEY (id_item_type) REFERENCES public.item_type(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: item fk_item_types_items; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.item
    ADD CONSTRAINT fk_item_types_items FOREIGN KEY (id_item_type) REFERENCES public.item_type(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: document fk_items_documents; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.document
    ADD CONSTRAINT fk_items_documents FOREIGN KEY (id_item) REFERENCES public.item(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: local_ties fk_local_ties_stations; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.local_ties
    ADD CONSTRAINT fk_local_ties_stations FOREIGN KEY (id_station) REFERENCES public.station(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: log fk_log_types_logs; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.log
    ADD CONSTRAINT fk_log_types_logs FOREIGN KEY (id_log_type) REFERENCES public.log_type(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: network fk_network_contact; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.network
    ADD CONSTRAINT fk_network_contact FOREIGN KEY (pid) REFERENCES public.contact(pid);


--
-- Name: station_network fk_network_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.station_network
    ADD CONSTRAINT fk_network_id FOREIGN KEY (id_network) REFERENCES public.network(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: node_data_center fk_node_data_center; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.node_data_center
    ADD CONSTRAINT fk_node_data_center FOREIGN KEY (id_data_center) REFERENCES public.data_center(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: node_data_center fk_node_data_center_data_center; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.node_data_center
    ADD CONSTRAINT fk_node_data_center_data_center FOREIGN KEY (id_data_center) REFERENCES public.data_center(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: node_data_center fk_node_data_center_node; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.node_data_center
    ADD CONSTRAINT fk_node_data_center_node FOREIGN KEY (id_node) REFERENCES public.node(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: qc_constellation_summary fk_qc_constellation_summary_constellation; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_constellation_summary
    ADD CONSTRAINT fk_qc_constellation_summary_constellation FOREIGN KEY (id_constellation) REFERENCES public.constellation(id);


--
-- Name: qc_constellation_summary fk_qc_constellation_summary_qc_report_summary; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_constellation_summary
    ADD CONSTRAINT fk_qc_constellation_summary_qc_report_summary FOREIGN KEY (id_qc_report_summary) REFERENCES public.qc_report_summary(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: qc_navigation_msg fk_qc_navigation_msg_constellation; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_navigation_msg
    ADD CONSTRAINT fk_qc_navigation_msg_constellation FOREIGN KEY (id_constellation) REFERENCES public.constellation(id);


--
-- Name: qc_navigation_msg fk_qc_navigation_msg_qc_report_summary; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_navigation_msg
    ADD CONSTRAINT fk_qc_navigation_msg_qc_report_summary FOREIGN KEY (id_qc_report_summary) REFERENCES public.qc_report_summary(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: qc_observation_summary_c fk_qc_observation_summary_c_gnss_obsnames; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_observation_summary_c
    ADD CONSTRAINT fk_qc_observation_summary_c_gnss_obsnames FOREIGN KEY (id_gnss_obsnames) REFERENCES public.gnss_obsnames(id);


--
-- Name: qc_observation_summary_c fk_qc_observation_summary_c_qc_constellation_summary; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_observation_summary_c
    ADD CONSTRAINT fk_qc_observation_summary_c_qc_constellation_summary FOREIGN KEY (id_qc_constellation_summary) REFERENCES public.qc_constellation_summary(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: qc_observation_summary_d fk_qc_observation_summary_d_gnss_obsnames; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_observation_summary_d
    ADD CONSTRAINT fk_qc_observation_summary_d_gnss_obsnames FOREIGN KEY (id_gnss_obsnames) REFERENCES public.gnss_obsnames(id);


--
-- Name: qc_observation_summary_d fk_qc_observation_summary_d_qc_constellation_summary; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_observation_summary_d
    ADD CONSTRAINT fk_qc_observation_summary_d_qc_constellation_summary FOREIGN KEY (id_qc_constellation_summary) REFERENCES public.qc_constellation_summary(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: qc_observation_summary_l fk_qc_observation_summary_l_gnss_obsnames; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_observation_summary_l
    ADD CONSTRAINT fk_qc_observation_summary_l_gnss_obsnames FOREIGN KEY (id_gnss_obsnames) REFERENCES public.gnss_obsnames(id);


--
-- Name: qc_observation_summary_l fk_qc_observation_summary_l_qc_constellation_summary; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_observation_summary_l
    ADD CONSTRAINT fk_qc_observation_summary_l_qc_constellation_summary FOREIGN KEY (id_qc_constellation_summary) REFERENCES public.qc_constellation_summary(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: qc_observation_summary_s fk_qc_observation_summary_s_gnss_obsnames; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_observation_summary_s
    ADD CONSTRAINT fk_qc_observation_summary_s_gnss_obsnames FOREIGN KEY (id_gnss_obsnames) REFERENCES public.gnss_obsnames(id);


--
-- Name: qc_observation_summary_s fk_qc_observation_summary_s_qc_constellation_summary; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_observation_summary_s
    ADD CONSTRAINT fk_qc_observation_summary_s_qc_constellation_summary FOREIGN KEY (id_qc_constellation_summary) REFERENCES public.qc_constellation_summary(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: qc_report_summary fk_qc_report_file; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_report_summary
    ADD CONSTRAINT fk_qc_report_file FOREIGN KEY (id_rinexfile) REFERENCES public.rinex_file(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: qc_report_summary fk_qc_report_qc_parameters; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qc_report_summary
    ADD CONSTRAINT fk_qc_report_qc_parameters FOREIGN KEY (id_qc_parameters) REFERENCES public.qc_parameters(id);


--
-- Name: highrate_rinex_errors fk_rinex_errors_highrate; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.highrate_rinex_errors
    ADD CONSTRAINT fk_rinex_errors_highrate FOREIGN KEY (id_rinex_file_highrate) REFERENCES public.highrate_rinex_file(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: highrate_rinex_errors fk_rinex_errors_highrate_error_type; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.highrate_rinex_errors
    ADD CONSTRAINT fk_rinex_errors_highrate_error_type FOREIGN KEY (id_error_type) REFERENCES public.highrate_rinex_types(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: rinex_errors fk_rinex_errors_rinex_error_types; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rinex_errors
    ADD CONSTRAINT fk_rinex_errors_rinex_error_types FOREIGN KEY (id_error_type) REFERENCES public.rinex_error_types(id);


--
-- Name: rinex_errors fk_rinex_errors_rinex_file; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rinex_errors
    ADD CONSTRAINT fk_rinex_errors_rinex_file FOREIGN KEY (id_rinex_file) REFERENCES public.rinex_file(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: rinex_file fk_rinex_file_data_center_structure; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rinex_file
    ADD CONSTRAINT fk_rinex_file_data_center_structure FOREIGN KEY (id_data_center_structure) REFERENCES public.data_center_structure(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: rinex_file fk_rinex_file_file_type; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rinex_file
    ADD CONSTRAINT fk_rinex_file_file_type FOREIGN KEY (id_file_type) REFERENCES public.file_type(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: highrate_rinex_file fk_rinex_file_highrate_data_center_structure; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.highrate_rinex_file
    ADD CONSTRAINT fk_rinex_file_highrate_data_center_structure FOREIGN KEY (id_data_center_structure) REFERENCES public.data_center_structure(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: highrate_rinex_file fk_rinex_file_highrate_file_type; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.highrate_rinex_file
    ADD CONSTRAINT fk_rinex_file_highrate_file_type FOREIGN KEY (id_file_type) REFERENCES public.file_type(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: highrate_rinex_file fk_rinex_file_highrate_station; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.highrate_rinex_file
    ADD CONSTRAINT fk_rinex_file_highrate_station FOREIGN KEY (id_station) REFERENCES public.station(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: role fk_role_contact; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT fk_role_contact FOREIGN KEY (contact_pid) REFERENCES public.contact(pid);


--
-- Name: condition fk_station; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.condition
    ADD CONSTRAINT fk_station FOREIGN KEY (id_station) REFERENCES public.station(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: station fk_station_bedrock; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.station
    ADD CONSTRAINT fk_station_bedrock FOREIGN KEY (id_bedrock) REFERENCES public.bedrock(id);


--
-- Name: station fk_station_country; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.station
    ADD CONSTRAINT fk_station_country FOREIGN KEY (country_code) REFERENCES public.country(iso_code);


--
-- Name: station_network fk_station_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.station_network
    ADD CONSTRAINT fk_station_id FOREIGN KEY (id_station) REFERENCES public.station(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: station_item fk_station_item_item; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.station_item
    ADD CONSTRAINT fk_station_item_item FOREIGN KEY (id_item) REFERENCES public.item(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: station_item fk_station_item_station; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.station_item
    ADD CONSTRAINT fk_station_item_station FOREIGN KEY (id_station) REFERENCES public.station(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: document fk_stations_documents; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.document
    ADD CONSTRAINT fk_stations_documents FOREIGN KEY (id_station) REFERENCES public.station(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: log fk_stations_logs; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.log
    ADD CONSTRAINT fk_stations_logs FOREIGN KEY (id_station) REFERENCES public.station(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: agency pid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.agency
    ADD CONSTRAINT pid FOREIGN KEY (pid) REFERENCES public.contact(pid);


--
-- Name: CONSTRAINT pid ON agency; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON CONSTRAINT pid ON public.agency IS 'persistent identifier';


--
-- Name: stations_with_files_summary; Type: MATERIALIZED VIEW DATA; Schema: public; Owner: -
--

REFRESH MATERIALIZED VIEW public.stations_with_files_summary;


--
-- Name: stations_with_highrate_files_summary; Type: MATERIALIZED VIEW DATA; Schema: public; Owner: -
--

REFRESH MATERIALIZED VIEW public.stations_with_highrate_files_summary;


--
-- PostgreSQL database dump complete
--

